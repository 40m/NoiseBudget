from header import *

nds_server = 'nds40.ligo.caltech.edu'
nds_port = 31200

darm_date = '2017-10-08'
darm_start = 1191483071
darm_stop = 1191484125
darm_oltf_path = 'Data/MICH_OLTF_modelled_20171008.mat'

filtFileName = '/opt/rtcds/caltech/c1/chans/C1SUS.txt'

opt_gain_ref = 2.04e12 # cts/m, from sensing matrix measurement...
darm_pole_ref = 341 # Hz

wavelength = 1064e-9 # m
power = 1 # W
prgain = 23 # W/W
arm_length = 37.79 # m
mass = 0.25 # kg
arm_trans = 0.01384   #ITM transmissivity
srm_trans = 0.09903   #SRM transmissivity

#######################################
# One-off functions
#######################################
def get_shot(ff, AS55_sum, demodAngle=90., lam=1064e-9, m2W=2.3e-6):
	return ff, np.ones(len(ff))* np.sqrt(2 * scc.h * (scc.c/lam) * 
			AS55_sum * 1e-3 * (0.5 -0.25*np.cos(2*np.deg2rad(demodAngle)))) * m2W
	#(Cyclostationary) Shot noise formula as per Tobin's thesis eq. 2.17

def get_dark_noise(fname, pdname):
    data = scio.loadmat(fname)
    ff = data['f_'+pdname][0,:]
    pdark = data['p_'+pdname][0,:]
    return ff, np.sqrt(pdark)

def get_dac_noise(ff, a=150.,b=80.,c=150., R=400., nCoils=4.,i2f=0.016,deWhite=False):
	'''
	Computes DAC noise according to a model
	n_DAC = sqrt(a^2 * (b/f) + c^2) nV/rtHz
	Converts this to a displacement noise by assuming an actuator gain of
	0.016 N/A for each OSEM coil (add noise in quadrature). Option to filter
	the DAC noise through the analog de-whitening installed.
	'''
	dacNoise = 1e-9 * np.sqrt((a**2)*(b/ff) + c**2) # V/rtHz
	if deWhite:
		ps = 2*np.pi*np.array([11.1103+1j*11.107, 11.1103-1j*11.107,
			10.0125+1j*10.3911, 10.0125-1j*10.3911,13.78, 3194.66])
		zs = 2*np.pi*np.array([65.8755+1j*77.3235, 65.8755-1j*77.3235,
			76.0471+1j*60.1505, 76.0471-1j*60.1505, 133.7, 526.1])
		k = 1.0403 * np.abs(np.prod(ps)) / np.abs(np.prod(zs))
		w,H = sig.freqs_zpk(zs, ps, k, 2*np.pi*ff)
		dacNoise = dacNoise * np.abs(H)
		lisoNoiseDat = np.loadtxt('Data/lisoFiles/ITMXcoilDriver.out')
		lisoNoise = np.interp(ff,lisoNoiseDat[:,0], lisoNoiseDat[:,8])
		dacNoise = np.maximum(dacNoise, lisoNoise)
	dacNoise = dacNoise / R             # A/rtHz
	dacNoise = np.sqrt(nCoils) * dacNoise # Quadrature sum of all coils
	dacNoise = dacNoise * i2f     # N/rtHz
	dacNoise = dacNoise / (mass*(2*np.pi*ff)**2)         #m/rtHz
	return ff, dacNoise

def get_seismic(fname,ff):
    data = scio.loadmat(fname)
    fff = data['darmseis_f'][0,:]
    disp = data['darmseis_x'][0,:]
    noise_seis = 10**(np.interp(ff,fff,np.log10(disp)))  #This is displacement noise, not strain.
    return ff, noise_seis*np.sqrt(3) #quadrature sum of 3 suspended masses.


def get_AUX_coupling(ff,chan,couplingTFmag):
	fTemp, dat = nbu.get_ifo_data_nds(chan,darm_start,darm_stop)
	cpl = np.interp(ff, fTemp, dat)
	cpl = couplingTFmag * cpl
	return ff, cpl


#######################################
# Noise dictionaries
#######################################
RFPD_dict = {
    'label': r'Measured',
    'function': nbu.get_ifo_data_nds,
    'args': ('C1:LSC-MICH_IN1_DQ',
        darm_start,
        darm_stop),
    'type': 'sensing',
    }
OL_BS_dict = {
    'label': r'OL Coupling (BS)',
	'group': r'A2L',
    'function': nbu.get_OL_coupling,
    'args': (np.logspace(0, 3, 2000),darm_start, darm_stop,['BS'],['PIT','YAW'],filtFileName,[0,1,4,8],55),
    'type': 'sensing',
    }
OL_ITMX_dict = {
    'label': r'OL Coupling (ITMX)',
	'group':'A2L',
    'function': nbu.get_OL_coupling,
    'args': (np.logspace(0, 3, 2000),darm_start, darm_stop,['ITMX'],['PIT','YAW'],filtFileName,[1,4,6,8],7),
    'type': 'sensing',
    }
OL_ITMY_dict = {
    'label': r'OL Coupling (ITMY)',
	'group':'A2L',
    'function': nbu.get_OL_coupling,
    'args': (np.logspace(0, 3, 2000),darm_start, darm_stop,['ITMY'],['PIT','YAW'],filtFileName,[1,4,6,8],12),
    'type': 'sensing',
    }

dark_dict = {
    'label': r'Dark noise',
    'function': get_dark_noise,
    'args': ('Data/RFPD_dark.mat','AS55'),
    'type': 'null'
    }

shot_dict = {
    'label': r'Shot noise',
    'group': r'Quantum noise',
    'function': get_shot,
    'args': (np.logspace(-1, 5, 300),
        2.60),
    'type': 'displacement',
    }

dac_BS_dict = {
    'label': r'BS DAC noise',
    'group': r'DAC noise',
    'function': get_dac_noise,
    'args': (np.logspace(-1, 4, 500),
        300, 100, 300, 100.),
	'kwargs':{'deWhite': True},
    'type': 'displacement',
    }
dac_ITMX_dict = {
    'label': r'ITMX DAC noise',
    'group': r'DAC noise',
    'function': get_dac_noise,
    'args': (np.logspace(-1, 4, 500),
        150, 80, 150),
	'kwargs':{'deWhite': True},
    'type': 'displacement',
    }
dac_ITMY_dict = {
    'label': r'ITMY DAC noise',
    'group': r'DAC noise',
    'function': get_dac_noise,
    'args': (np.logspace(-1, 4, 500),
        150, 80, 150),
	'kwargs':{'deWhite': True},
    'type': 'displacement',
    }

AUX_PRCL_dict = {
    'label': r'PRCL to MICH coupling',
    'group': r'AUX coupling',
    'function': get_AUX_coupling,
    'args': (np.logspace(-1, 4, 500),
		'C1:LSC-PRCL_IN1_DQ',0.007*np.ones(500)),
    'type': 'sensing',
    }

AUX_SRCL_dict = {
    'label': r'SRCL to MICH coupling',
    'group': r'AUX coupling',
    'function': get_AUX_coupling,
    'args': (np.logspace(-1, 4, 500),
		'C1:LSC-SRCL_IN1_DQ',0.06*np.ones(500)),
    'type': 'sensing',
    }

seismic_dict = {
    'label': 'Seismic noise',
    'group': 'Seismic+Newtonian',
    'function': get_seismic,
    'args': ('Data/GwincCurves/seis40.mat',np.logspace(-1,5,300)),
    'type': 'displacement',
    }

newtonian_dict = {
    'label': 'Newtonian noise',
    'group': 'Seismic+Newtonian',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/newtonianNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

mirror_thermal_dict = {
    'label': r'Mirror thermal noise',
    'group': r'Thermal',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/mirrorThermalNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

sus_thermal_dict = {
    'label': r'Suspension thermal noise',
    'group': r'Thermal',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/suspThermNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

res_gas_dict = {
    'label': r'Residual gas noise',
    'group': r'Gas noise',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/resGasNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }


trace_dict_list = [
        RFPD_dict,
        OL_BS_dict,
        OL_ITMX_dict,
        OL_ITMY_dict,
		shot_dict,
        dark_dict,
		dac_BS_dict,
		dac_ITMX_dict,
		dac_ITMY_dict,
		AUX_PRCL_dict,
        AUX_SRCL_dict,
        seismic_dict,
        newtonian_dict,
        mirror_thermal_dict,
        sus_thermal_dict,
        res_gas_dict,
        ]
