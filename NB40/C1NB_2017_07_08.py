from header import *

nds_server = 'nds40.ligo.caltech.edu'
nds_port = 31200

darm_date = '2017-07-08'
darm_start = 1183536595
darm_stop = 1183536993
darm_oltf_path = 'Data/MICH_OLTF_modelled.mat'


opt_gain_ref = 3.364e12 # cts/m, from sensing matrix measurement...
#opt_gain_ref = 3.526e12 # cts/m, from sensing matrix measurement...
						# For easy reference
						# 1.076e9 V/m
						#2.419e6 W/m
darm_pole_ref = 341 # Hz

wavelength = 1064e-9 # m
power = 1 # W
prgain = 23 # W/W
arm_length = 37.79 # m
mass = 0.25 # kg
arm_trans = 0.01384   #ITM transmissivity
srm_trans = 0.09903   #SRM transmissivity


#dac_noise = lambda ff: 300e-9*np.sqrt(1+(50./ff)**2)

#######################################
# One-off functions
#######################################
def get_shot(ff, AS55_sum):
    return ff, np.ones(len(ff))*np.sqrt(2*scc.e*AS55_sum*1e-3*0.8)*556*(2**15/10.)

def get_dark_noise(fname, pdname):
    data = scio.loadmat(fname)
    ff = data['f_'+pdname][0,:]
    pdark = data['p_'+pdname][0,:]
    return ff, np.sqrt(pdark)

def get_dac_noise(ff, a ,b ,c, scaleFactor):
	'''
	Computes DAC noise according to a model
	n_DAC = sqrt(a^2 * (b/f) + c^2) nV/rtHz
	'''
	dacNoise = 1e-9 * np.sqrt((a**2)*(b/ff) + c**2) #V/rtHz
	dacNoise = dacNoise * (2**15)/10.0             #cts/rtHz
	dacNoise = (dacNoise * 1.55e-9) / (ff**2)         #m/rtHz
	dacNoise = dacNoise * scaleFactor           #Accounting for 3 optics
	return ff, dacNoise

def get_seismic(fname,ff):
    data = scio.loadmat(fname)
    fff = data['darmseis_f'][0,:]
    disp = data['darmseis_x'][0,:]
    noise_seis = 10**(np.interp(ff,fff,np.log10(disp)))  #This is displacement noise, not strain.
    return ff, noise_seis*np.sqrt(3) #quadrature sum of 3 suspended masses.
'''
def get_OL_coupling(fname, opticname, DoFname):
    ff = np.logspace(0, 3, 2000)
    data = scio.loadmat('Data/INLOCK_OL_MICH_DRMI.mat')
    fs = 16384
    t_fft = 20
    nperseg = t_fft*fs
    window = sig.get_window('hanning', nperseg)
    ff1, BS_P = sig.welch(data['BS_P'][:,0],fs, window, int(nperseg))
    ff2, BS_Y = sig.welch(data['BS_Y'][:,0],fs, window, int(nperseg))
    ff3, ITMX_P = sig.welch(data['ITMX_P'][:,0],fs, window, int(nperseg))
    ff4, ITMX_Y = sig.welch(data['ITMX_Y'][:,0],fs, window, int(nperseg))
    ff5, ITMY_P = sig.welch(data['ITMY_P'][:,0],fs, window, int(nperseg))
    ff6, ITMY_Y = sig.welch(data['ITMY_Y'][:,0],fs, window, int(nperseg))
    BS_P = np.interp(ff,ff1,BS_P)
    BS_Y = np.interp(ff,ff2,BS_Y)
    ITMX_P = np.interp(ff,ff3,ITMX_P)
    ITMX_Y = np.interp(ff,ff4,ITMX_Y)
    ITMY_P = np.interp(ff,ff5,ITMY_P)
    ITMY_Y = np.interp(ff,ff6,ITMY_Y)
    #Load TFs...
    BS_P_TF = np.loadtxt('Data/OL_coupling_TFs/BS_P.txt')
    BS_P_TF_mag = np.interp(ff,BS_P_TF[:,0], np.sqrt(BS_P_TF[:,1]**2 + BS_P_TF[:,2]**2))
    BS_P = np.sqrt(BS_P)*BS_P_TF_mag

    BS_Y_TF = np.loadtxt('Data/OL_coupling_TFs/BS_Y.txt')
    BS_Y_TF_mag = np.interp(ff,BS_Y_TF[:,0], np.sqrt(BS_Y_TF[:,1]**2 + BS_Y_TF[:,2]**2))
    BS_Y = np.sqrt(BS_Y)*BS_Y_TF_mag

    ITMX_P_TF = np.loadtxt('Data/OL_coupling_TFs/ITMX_P.txt')
    ITMX_P_TF_mag = np.interp(ff,ITMX_P_TF[:,0], np.sqrt(ITMX_P_TF[:,1]**2 + ITMX_P_TF[:,2]**2))
    ITMX_P = np.sqrt(ITMX_P)*ITMX_P_TF_mag

    ITMX_Y_TF = np.loadtxt('Data/OL_coupling_TFs/ITMX_Y.txt')
    ITMX_Y_TF_mag = np.interp(ff,ITMX_Y_TF[:,0], np.sqrt(ITMX_Y_TF[:,1]**2 + ITMX_Y_TF[:,2]**2))
    ITMX_Y = np.sqrt(ITMX_Y)*ITMX_Y_TF_mag

    ITMY_P_TF = np.loadtxt('Data/OL_coupling_TFs/ITMY_P.txt')
    ITMY_P_TF_mag = np.interp(ff,ITMY_P_TF[:,0], np.sqrt(ITMY_P_TF[:,1]**2 + ITMY_P_TF[:,2]**2))
    ITMY_P = np.sqrt(ITMY_P)*ITMY_P_TF_mag

    ITMY_Y_TF = np.loadtxt('Data/OL_coupling_TFs/ITMY_Y.txt')
    ITMY_Y_TF_mag = np.interp(ff,ITMY_Y_TF[:,0], np.sqrt(ITMY_Y_TF[:,1]**2 + ITMY_Y_TF[:,2]**2))
    ITMY_Y = np.sqrt(ITMY_Y)*ITMY_Y_TF_mag

    pMICH_OL_coh = np.sqrt(BS_P**2 + BS_Y**2 + ITMX_P**2 + ITMX_Y**2 + ITMY_P**2 + ITMY_Y**2)
    return ff, pMICH_OL_coh
'''

def get_OL_coupling(ff, fname, opticname, DoFname):
    data = scio.loadmat(fname)
    fs = 16384
    t_fft = 20
    nperseg = t_fft*fs
    window = sig.get_window('hanning', nperseg)
    pMICH_OL_coh = 0
    for ii in range(0,len(opticname)):
        for jj in range(0,len(DoFname)):
            f_temp, psd_temp = sig.welch(data[opticname[ii]+'_'+DoFname[jj]][:,0],
            fs, window, int(nperseg))
            psd_temp = np.interp(ff,f_temp, psd_temp)
            TF_temp = np.loadtxt('Data/OL_coupling_TFs/'+opticname[ii]+'_'+DoFname[jj]+'.txt')
            TF_temp_mag = np.interp(ff, TF_temp[:,0], np.sqrt(TF_temp[:,1]**2 + TF_temp[:,2]**2))
            psd_proj = np.sqrt(psd_temp)*TF_temp_mag
            pMICH_OL_coh += psd_proj**2
    pMICH_OL_coh = np.sqrt(pMICH_OL_coh)
    return ff, pMICH_OL_coh


#######################################
# Noise dictionaries
#######################################
RFPD_dict = {
    'label': r'Measured',
    'function': nbu.get_ifo_data_nds,
    'args': ('C1:LSC-MICH_IN1_DQ',
        darm_start,
        darm_stop),
    'type': 'sensing',
    }
'''
OL_BS_dict = {
    'label': r'OL Coupling (BS)',
    'function': nbu.get_OL_coupling,
    'args':(darm_start,
        darm_stop,
        ['C1:SUS-BS_OPLEV_PERROR','C1:SUS-BS_OPLEV_YERROR'],
        np.array([97.0, 96.5]),
        np.array([-110.0, -110])),
    'type': 'sensing',
    }

OL_ITM_dict = {
    'label': r'OL Coupling (ITM)',
    'function': nbu.get_OL_coupling,
    'args':(darm_start,
        darm_stop,
        ['C1:SUS-ITMX_OPLEV_PERROR','C1:SUS-ITMX_OPLEV_YERROR',
        'C1:SUS-ITMY_OPLEV_PERROR','C1:SUS-ITMY_OPLEV_YERROR'],
        np.array([55.0, 43.5, 56.2, 47.5]),
        np.array([110.0, -56.0, -70.0, -56.0])),
    'type': 'sensing',
    }
'''
OL_BS_dict = {
    'label': r'OL Coupling (BS)',
    'function': get_OL_coupling,
    'args': (np.logspace(0, 3, 2000), 'Data/INLOCK_OL_MICH_DRMI.mat',['BS'],['P','Y']),
    'type': 'sensing',
    }
OL_ITMX_dict = {
    'label': r'OL Coupling (ITMX)',
    'function': get_OL_coupling,
    'args': (np.logspace(0, 3, 2000), 'Data/INLOCK_OL_MICH_DRMI.mat',['ITMX'],['P','Y']),
    'type': 'sensing',
    }
OL_ITMY_dict = {
    'label': r'OL Coupling (ITMY)',
    'function': get_OL_coupling,
    'args': (np.logspace(0, 3, 2000), 'Data/INLOCK_OL_MICH_DRMI.mat',['ITMY'],['P','Y']),
    'type': 'sensing',
    }

dark_dict = {
    'label': r'Dark noise',
    'function': get_dark_noise,
    'args': ('Data/RFPD_dark.mat','AS55'),
    'type': 'null'
    }

shot_dict = {
    'label': r'Shot noise',
    'group': r'Quantum noise',
    'function': get_shot,
    'args': (np.logspace(-1, 5, 300),
        13.0,
        ),
    'type': 'null',
    }


dac_dict = {
    'label': r'DAC noise',
    'group': r'DAC noise',
    'function': get_dac_noise,
    'args': (np.logspace(-1, 4, 500),
        300, 100, 300, np.sqrt(6),
        ),
    'type': 'displacement',
    }

seismic_dict = {
    'label': 'Seismic noise',
    'group': 'Seismic+Newtonian',
    'function': get_seismic,
    'args': ('Data/GwincCurves/seis40.mat',np.logspace(-1,5,300)),
    'type': 'displacement',
    }

newtonian_dict = {
    'label': 'Newtonian noise',
    'group': 'Seismic+Newtonian',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/newtonianNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

mirror_thermal_dict = {
    'label': r'Mirror thermal noise',
    'group': r'Thermal',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/mirrorThermalNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

sus_thermal_dict = {
    'label': r'Suspension thermal noise',
    'group': r'Thermal',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/suspThermNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

res_gas_dict = {
    'label': r'Residual gas noise',
    'group': r'Gas noise',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/resGasNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }


trace_dict_list = [
        RFPD_dict,
        #OL_BS_dict,
        #OL_ITMX_dict,
        #OL_ITMY_dict,
		shot_dict,
        dark_dict,
		dac_dict,
        seismic_dict,
        newtonian_dict,
        mirror_thermal_dict,
        sus_thermal_dict,
        res_gas_dict,
        ]
