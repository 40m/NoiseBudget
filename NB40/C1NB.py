# Noise budget for 40m
# Usage: python C1NB.py C1NB_param_file.py

import importlib
import pickle
import matplotlib.ticker as ticker

from header import *


mpl.rcParams['text.usetex'] = True
usetex = mpl.rcParams['text.usetex']
# Import parameter file
par = importlib.import_module(sys.argv[1].replace('.py',''))
#par = importlib.import_module('C1NB_2017_04_30')


#darm_start_fname = par.darm_start.replace(':', '').replace(' ','_')
#darm_start_fname = '40m_MICH_NB_6_Jul_2017'
darm_start_fname = '40m_MICH_NB_'+par.darm_date
bdir = 'Budgets/'+darm_start_fname+'/'
if not os.path.exists(bdir):
    os.mkdir(bdir)

# Set up log
#logpath = bdir+sys.argv[1].replace('.py', '_log.txt')
logpath = './log.txt'
flog = open(logpath, 'w')
def log(string):
    print(string)
    flog.write(string+'\n')

opt_gain = par.opt_gain_ref

# Set up plots
hh_disp = plt.figure(figsize=(6,5))
ax_disp = hh_disp.add_subplot(111)
hh_res = plt.figure(figsize=(8,6))
ax_res = hh_res.add_subplot(111)
hh_res_hp = plt.figure(figsize=(8,6))
ax_res_hp = hh_res_hp.add_subplot(111)
hh_sensing = plt.figure(figsize=(8,6))
ax_sensing = hh_sensing.add_subplot(111)


trace_list = []
label_list = []
grouped_traces_dict = {}
grouped_labels_list = []

# Process the data using the chosen parameter file
for ii, trace_dict in enumerate(par.trace_dict_list):
    log('Now processing {}'.format(trace_dict['label']))
    func = trace_dict['function']
    if 'args' in trace_dict.keys():
       if 'kwargs' in trace_dict.keys():
           f0, trace = func(*trace_dict['args'], **trace_dict['kwargs'])
       else:
           f0, trace = func(*trace_dict['args'])
    else:
        f0, trace = func()
    # Interpolate all other traces onto the first trace (assumed to the measured displacement noise)
    if ii==0:
        ff = f0
    else:
        trace = np.interp(ff, f0, trace)
    if 'group' not in trace_dict.keys():
        trace_dict['group'] = trace_dict['label']
    trace_group = trace_dict['group']
    if trace_group not in grouped_labels_list:
        grouped_labels_list.append(trace_group)
    if trace_group not in grouped_traces_dict.keys():
        grouped_traces_dict[trace_group] = np.zeros(np.shape(ff))
    # Now start plotting. We need to differentiate between
    # displacement and sensing traces here.
    if trace_dict['type'] == 'displacement':
        trace_dict['data'] = trace
        trace_list.append(trace)
        grouped_traces_dict[trace_group] = np.sqrt(grouped_traces_dict[trace_group]**2+trace**2)
    elif trace_dict['type'] == 'sensing':
        # Undo the loop transfer function and optical plant
        darm_oltf_interp = nbu.get_complex_interp(par.darm_oltf_path, ff)
        trace_disp = trace*np.abs((1-darm_oltf_interp)/opt_gain)
        #ax_disp.loglog(ff, trace_disp, label=trace_dict['label'], alpha=0.8)
        trace_dict['data'] = trace_disp
        trace_list.append(trace_disp)
        grouped_traces_dict[trace_group] = np.sqrt(grouped_traces_dict[trace_group]**2+trace_disp**2)
    elif trace_dict['type'] == 'null':
        # Undo the optical plant
        #ax_sensing.loglog(ff, trace, label=trace_dict['label'], alpha=0.8)
        trace_disp = trace/opt_gain
        trace_dict['data'] = trace_disp
        trace_list.append(trace_disp)
        grouped_traces_dict[trace_group] = np.sqrt(grouped_traces_dict[trace_group]**2+trace_disp**2)
    else:
        print('Trace type not understood')
    label_list.append(trace_dict['label'])

#for label, trace in zip(grouped_traces_dict.keys(), grouped_traces_dict.values()):
#    ax_disp.loglog(ff, trace, label=label, alpha=0.8)
grouped_traces_list = []
for ii, label in enumerate(grouped_labels_list):
    trace = grouped_traces_dict[label]
    ax_disp.loglog(ff, trace, label=label, alpha=0.8, zorder=len(grouped_labels_list)-ii)
    grouped_traces_list.append(trace)

measured_trace = trace_list[0]
total_trace = np.sqrt(np.sum(np.array(trace_list[1:])**2, axis=0))
trace_list.append(total_trace)
label_list.append('Total expected')
trace_list.insert(0, ff)
label_list.insert(0, 'Frequency')
grouped_traces_list.append(total_trace)
grouped_labels_list.append('Total expected')
grouped_traces_list.insert(0, ff)
grouped_labels_list.insert(0, 'Frequency')

ax_disp.loglog(ff, total_trace, label='Total expected', alpha=0.8)
res_raw = np.sqrt(np.abs(measured_trace**2-total_trace**2))
mask = (np.abs(res_raw/measured_trace) > 1./4)
res = mask*res_raw
#ax_disp.loglog(ff, res, label='Residual', alpha=0.9)

def power_law(ff, logA, a):
    return logA+a*np.log10(ff)

mask2 = np.all([ff>30, ff<60, res<1e-17], axis=0)
#q = opt.curve_fit(power_law, ff[mask2], np.log10(res[mask2]+1e-22),
#        p0=(-16, -2))

ax_res.loglog(ff, np.max([measured_trace, 0.9*total_trace], axis=0), alpha=0.7, label='Measured')
ax_res.loglog(ff, total_trace, alpha=0.7, label='Total expected')
#ax_res.loglog(ff, res, alpha=0.8, label='Residual')
af25 = 1.0e-19*(ff/30)**-2.5
af2 = 1e-20*(ff/100)**-2
#ax_res.loglog(ff, af25,
#        label='$A/f^{2.5}$')
meas_wo_mystery = np.max([np.sqrt(measured_trace**2-af2**2), 0.9*total_trace], axis=0)
meas_w_hp = np.sqrt(np.max([measured_trace, 0.9*total_trace], axis=0)**2 - 0.5*par.shot_dict['data']**2)
meas_wo_mystery_w_hp = np.sqrt(meas_wo_mystery**2 - 0.5*par.shot_dict['data']**2)
ax_res.loglog(ff, meas_wo_mystery, label=r'Measured -- $A/f^2$', alpha=0.7)
ax_res.loglog(ff, af2, label='$A/f^2$')
ax_res_hp.loglog(ff, meas_w_hp, label=r'Measured -- $0.5\times$shot', alpha=0.7)
ax_res_hp.loglog(ff, np.sqrt(total_trace**2- 0.5*par.shot_dict['data']**2), alpha=0.7,
        label=r'Total expected -- $0.5\times$shot')
ax_res_hp.loglog(ff, meas_wo_mystery_w_hp, label=r'Measured -- $0.5\times$shot -- $A/f^2$', alpha=0.7)
ax_res_hp.loglog(ff, af2, label='$A/f^2$')
np.savetxt('C1NB_projections.txt', np.c_[ff, measured_trace, meas_wo_mystery, meas_w_hp, meas_wo_mystery_w_hp])
ax_res.set_xlim(9, 600)
ax_res.set_ylim(9e-21, 1.5e-18)
ax_res.set_xlabel('Frequency [Hz]')
ax_res_hp.set_xlim(9, 600)
ax_res_hp.set_ylim(9e-21, 1.5e-18)
ax_res_hp.set_xlabel('Frequency [Hz]')
if usetex == True:
    ax_res.set_ylabel(r'ASD of displacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
    #ax_res.set_title(r'aLIGO H1 freerunning DARM, '
    #    '{} Z'.format(par.darm_start.replace('-', '--')))
    ax_res_hp.set_ylabel(r'\textsc{asd} of displacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
    #ax_res_hp.set_title(r'a\textsc{{ligo}} H1 freerunning \textsc{{darm}}, '
    #    '{} Z'.format(par.darm_start.replace('-', '--')))
else:
    ax_res.set_ylabel(r'ASD of displacement [m/Hz$^{1/2}$]')
    ax_res.set_title(r'aLIGO H1 freerunning DARM, {} Z'.format(par.darm_start))
ax_res.grid('on', which='both')
ax_res.grid(ls='solid', which='major')
ax_res.legend(loc='best', ncol=2)
ax_res_hp.grid('on', which='both')
ax_res_hp.grid(ls='solid', which='major')
ax_res_hp.legend(loc='best', ncol=2)

ax_disp.set_xlim(9, 5e3)
ax_disp.set_ylim(1e-20, 5e-11)
ax_disp.yaxis.set_major_locator(ticker.LogLocator(base=10.0, numticks=10))
ax_disp.yaxis.set_minor_locator(ticker.LogLocator(base=10.0,subs=(2,3,4,5,6,7,8,9,
                                                                20,30,40,50,60,70,80,90)))
ax_disp.set_xlabel('Frequency [Hz]')
if usetex == True:
    ax_disp.set_ylabel(r'ASD of displacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
#    ax_disp.set_title(r'aLIGO H1 freerunning DARM, '
#        '{} Z'.format(par.darm_start.replace('-', '--')))
else:
    ax_disp.set_ylabel(r'ASD of displacement [m/Hz$^{1/2}$]')
#    ax_disp.set_title(r'aLIGO H1 freerunning DARM, {} Z'.format(par.darm_start))
ax_disp.grid('on', which='both')
ax_disp.grid(alpha=0.1, which='minor')
#ax_disp.grid(ls='solid', which='major')
ax_disp.legend(loc='best', ncol=2).set_zorder(1000)

hh_disp.tight_layout()
hh_res.tight_layout()
hh_res_hp.tight_layout()
hh_sensing.tight_layout()

hh_disp.savefig(bdir+'C1NB_disp_{}.pdf'.format(darm_start_fname))
hh_res.savefig(bdir+'C1NB_residual_{}.pdf'.format(darm_start_fname))
hh_res_hp.savefig(bdir+'C1NB_residual_hp_{}.pdf'.format(darm_start_fname))
hh_sensing.savefig(bdir+'C1NB_sensing_{}.pdf'.format(darm_start_fname))

log('Writing data to file...')
label_list[0] += ' [Hz]'
label_list[1:] = [label + ' [m/rtHz]' for label in label_list[1:]]
grouped_labels_list[0] += ' [Hz]'
grouped_labels_list[1:] = [label + ' [m/rtHz]' for label in grouped_labels_list[1:]]
fileheader_full = '\n'.join(['H1 DARM noise budget from {} to {} Z.'.format(par.darm_start, par.darm_stop),
                        'Traces as follows:']
                        +[4*' ' + '[{:02d}] '.format(ind) + label for (ind, label) in enumerate(label_list)])
fileheader_grouped = '\n'.join(['H1 DARM noise budget from {} to {} Z.'.format(par.darm_start, par.darm_stop),
                        'Traces as follows:']
                        +[4*' ' + '[{:02d}] '.format(ind) + label for (ind, label) in enumerate(grouped_labels_list)])
np.savetxt(bdir+sys.argv[1].replace('.py', '_data_full.txt'),
        np.transpose(np.array(trace_list)),
        header=fileheader_full)
np.savetxt(bdir+sys.argv[1].replace('.py', '_data_grouped.txt'),
        np.transpose(np.array(grouped_traces_list)),
        header=fileheader_grouped)
pickle.dump(ax_disp, open(bdir+sys.argv[1].replace('.py', '_plot.pickle'), 'wb'))
