function n = seismic_40(f,ifo)
% SEISMIC - seismic noise psd at frequencies f for given ifo.
%
% Modified to include realistic SEI + SUS models (Rana, 11/2005)
%
% adapted for 40m   (Rana, Dec 08)

  % Interpolate the log10 onto the ifo frequency array

  % Load seismic data from SEI + SUS models
  load seis40

  % Interpolate the log10 onto the Bench frequency array
  n = interp1(darmseis_f, log10(darmseis_x), f, 'pchip', -30);

  % Convert into Strain
  n = 10.^(n) / ifo.Infrastructure.Length;

  % Convert to PSD from ASD
  n = n.^2;


