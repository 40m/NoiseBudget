% BEAMSIZES calculates ROCs and Beam radii in the 40m
lambda = 1064e-9;
c = 299792458;

Ritm = 1e6;
Retm = 57.1;

Larm = 38.55;

f_fsr = c/2/Larm;

g1 = 1 - Larm./Ritm;
g2 = 1 - Larm./Retm;

witm = sqrt(Larm*lambda/pi * sqrt(g2/(g1*(1-g1*g2))))
wetm = sqrt(Larm*lambda/pi * sqrt(g1/(g2*(1-g1*g2))))

dmn = f_fsr/pi * acos(sqrt(g1*g2))


Lprc = 8.328;        % from Upgrade doc
Lsrc = 6.662;

zR = pi*witm^2 / lambda;
wprm = witm*sqrt(1+(Lprc/zR)^2)
wsrm = witm*sqrt(1+(Lsrc/zR)^2)

Rprm = Lprc + zR^2 / Lprc
Rsrm = Lsrc + zR^2 / Lsrc



