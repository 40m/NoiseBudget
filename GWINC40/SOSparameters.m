% 40m SOS parameters 

% measured frequencies (bw=3mHz)
%
% pendulum = 1
% side     = 1
% pitch    = 0.6
% yaw      = 0.7
% vertical = 16
% roll     = 16 * sqrt(2)
% violin   = 


R = 0.075 / 2;   		        % optics radius, m 

H = 0.025;			             % optics thickness, m 

silicaRho = 2.2e3;               % density of fused silica (Kg/m^3);

M = pi * R^2 * H * silicaRho;    % mass, kg

J = M*(3*R^2+H^2)/12;		     % moment of inertia for pitch and yaw, kg*m^2

Jx = M*R^2/2;		             % moment of inertia for rotation

g = 9.81;			             % m/s^2

rhow = 7.8e3;			         % steel density, kg/m^3

r = 0.0017/2 * .0254;	         % wire radius, m (0.00085")

A = pi * r^2;		             % wire area cross section

rho = rhow * A;			         % mass per unit length

h = 6.77e-3;			         % vert distance from wire end to CoMass 

l = 0.248;		                 % vert distance from top of wire to CoMass

dcm = 1e-3;			             % diamter of wire standoff

b = sqrt(R^2-h.^2); %+dcm;	     % half dist between bottom attach points

E = 1.65e11;		             % Young's modulus (steel)

I = 0.25 * pi * r^4;		     % area moment of inertia

phiw = 1.7e-4;			         % loss angle for wire

EI = E*I*(1+i*phiw);		     % complex parameter used in formulas

a = 21.3e-3/2;			         % half distance between top attachment
                                 % points (from 40m CDD)
            
L = sqrt((b-a).^2+(l-h).^2);     % wire length

alpha = atan((b-a)./(l-h));	     % wire angle with vertical

T = M*g*(l-h)/(2*L);	         % tension in each wire

alphate =  11.5e-6;              % 1/K, thermal expansion coefficient for steel,
                                 % H. Imai et al, Journal Phys E, Vol 14
                                 % 1981, pg 883
                                
c = 486;                         % J/kg/K, heat capacity per unit mass

kappa = 49;                      % W/m/kg, thermal conductivity

