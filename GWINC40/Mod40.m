function ifo = Mod40(varargin);
% MOD40 will return, by default, the ifo model
%
% This is adapted from IFOMODS by AJW
% updated to use the circa 2007 Bench format by Rana
%
% the function of this script is to injest a LIGO-2 IFOModel file
% and modify the physical parameters to more correctly model the 40m



% Input argument parsing  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ 
if nargin == 0
  error('Mod40.m needs an IFOModel file as an input.')
elseif nargin == 1
  disp('Processing IFOModel file...')
  ifo = varargin{1};
else
  error('Too Many input arguments. Am confused. I Quit.')
end
% ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ 



% ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ 
ifo.Infrastructure.Length   = 38.55;	% meters
pressureTorr = 1e-5;
ifo.Infrastructure.ResidualGas.pressure     = pressureTorr * 133;    % Pa;
% Pa;

ifo.Optics.SRM.CavityLength = 6.3;	% ITM to SRM distance in m

% MOS Parameters
ifo.Materials.MassRadius    = 0.125 / 2;	% m
ifo.Materials.MassThickness = 0.050;	% m

% SOS Parameters
ifo.Materials.MassRadius    = 0.075 / 2;	% m
ifo.Materials.MassThickness = 0.025;	% m

ifo.Materials.Substrate.Temp = 293;

ifo.Seismic.darmSeiSusFile = 'seis40.mat';  % .mat file containing predictions for darm displacement

ifo.Optics.Loss             = 10e-6; % average per mirror power loss
ifo.Optics.Curvature.ITM    = 1e5;              % ROC of ITM 
ifo.Optics.Curvature.ETM    = 57.375;           % ROC of ETM
g1 = 1 - ifo.Infrastructure.Length/ifo.Optics.Curvature.ITM;
g2 = 1 - ifo.Infrastructure.Length/ifo.Optics.Curvature.ETM;
w12 = ifo.Infrastructure.Length*ifo.Laser.Wavelength/pi*...
      sqrt(g2/(g1*(1-g1*g2)));
w22 = ifo.Infrastructure.Length*ifo.Laser.Wavelength/pi*...
      sqrt(g1/(g2*(1-g1*g2)));
ifo.Optics.ITM.BeamRadius   = sqrt(w12);           % m; 1/e^2 power radius 
ifo.Optics.ETM.BeamRadius   = sqrt(w22);           % m; 1/e^2 power radius 


ifo.Optics.SubstrateAbsorption = 2.5e-4;     % 1/m; bulk absorption coef (ref. 2)
ifo.Optics.pcrit = 10;                       % W; tolerable heating power (factor 1 ATC)
ifo.Optics.ITM.CoatingAbsorption = 3e-6;     % absorption of ITM 
ifo.Optics.ITM.Transmittance  = 0.014;       % Transmittance of ITM
ifo.Optics.ETM.Transmittance  = 10e-6;       % Transmittance of ETM 
ifo.Optics.SRM.Transmittance  = 0.1;         % Transmittance of SRM
ifo.Optics.PRM.Transmittance  = 0.056;

ifo.Optics.SRC.Loss                = 0.001;	   % SR mirror loss [Power]
ifo.Optics.PhotoDetectorEfficiency = 0.95;     % PD Q.E. 


ifo.Optics.Quadrature.dc = pi/2;        % demod/detection/homodyne phase
ifo.Optics.SRM.Tunephase = 0.7447;	    % SRM tuning for 4000 Hz peak

ifo.gwinc.dhdl_sqr = 1/ifo.Infrastructure.Length^2;

ifo.Laser.Power      = 1;		% W

return
