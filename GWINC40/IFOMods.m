function ifo =IFOMods(varargin);
% IFOMODS will return, by default, the ifo model
%  defined in IFOModel_200103,
%  with additional quantities required by AJW's Bench code
%  for modeling seismic, ResidGas, and single-pendulum noise.
%  Called with a numeric argument, it attempts to define 
%   iifo = 1 = LIGO I
%   iifo = 2 = LIGO II (sapphire)
%   iifo = 3 = LIGO II (silica)
%   iifo = 4 = 40m DR
%                                                  AJW, 3/01

iifo = 2;
if (nargin == 1)
  if (~isempty(varargin{1}))
    iifo = varargin{1};
  end
end

if (iifo == 2)
    ifo = IFOModel_200103;
else
    ifo = IFOModel_200103('SILICA');
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Additions by AJW in 3/01, all IFOs:

%Seismic noise Parameters:
% default for LIGO II:
ifo.Seismic.Exponent = 12;
ifo.Seismic.f0 = 1;         % Hz
ifo.Seismic.X0 = 1.17e-11;   % m, x(f<f0)

if (iifo == 1)    % LIGO I
    ifo.Seismic.Exponent = 11;
    ifo.Seismic.f0 = 10;         % Hz
    ifo.Seismic.X0 = 1.17e-11;   % m, x(f<f0)
elseif (iifo == 4)   % 40m
%    ifo.Seismic.X0 = 1e-10;        
    ifo.Seismic.X0 = 1.5e-12;        
%    ifo.Seismic.f0 = 21;          % Hz  (40m)
    ifo.Seismic.f0 = 10;          % Hz  (40m)
% two powers for each of 4 mas/spring stages in the passive seismic stack,
% two powers for the pendulum,
% and 2.5 powers for the natural seismic spectrum above 10 Hz:
%    ifo.Seismic.Exponent = 12.5;
    ifo.Seismic.Exponent = 8;
end

%%%%%%%%%%%%%%%
% Residual gas in the beam tubes, by molecular species.
% For LIGO, assume H2 dominates completely.
% For now, hydrocarbons are ignored, as negligible.
ifo.ResidGas(1).Amu = 2;                 % H2
ifo.ResidGas(1).nminus1 = 139e-6;        % index of refraction n-1
ifo.ResidGas(1).Pressure = 2.625e-9;     % torr
ifo.ResidGas(2).Amu = 18;                % H2O
ifo.ResidGas(2).nminus1 = 250e-6;        % index of refraction n-1
ifo.ResidGas(2).Pressure = 2.e-15;       % torr
ifo.ResidGas(3).Amu = 28;                % N2
ifo.ResidGas(3).nminus1 = 271e-6;        % index of refraction n-1
ifo.ResidGas(3).Pressure = 2.e-15;       % torr
ifo.ResidGas(4).Amu = 28;                % CO
ifo.ResidGas(4).nminus1 = 250e-6;        % index of refraction n-1 (??)
ifo.ResidGas(4).Pressure = 2.e-15;       % torr
ifo.ResidGas(5).Amu = 44;                % CO2
ifo.ResidGas(5).nminus1 = 400e-6;        % index of refraction n-1
ifo.ResidGas(5).Pressure = 2.e-15;       % torr


ifo.Constants.NA   = 6.02e26;	      % Avogadro's number (nucleons/kg)
ifo.Constants.Torr2Pascal = 133.3;	  % conversion
ifo.Constants.Vstp = 22.4;            % liters/mole for ideal gas

%%%%%%%%%%%%%%%%%%%%%%
% AJW: change for 40m:
if (iifo == 4)
  ifo.Infrastructure.Length = 38.25;	% meters
  ifo.Optics.SRM.CavityLength = 2.319;	% ITM to SRM distance in m

  ifo.Materials.MassRadius = 0.0625;	% m
  ifo.Materials.MassThickness = 0.050;	% m

  ifo.Materials.MassRadius = 0.075 / 2;	% m
  ifo.Materials.MassThickness = 0.025;	% m

  ifo.Optics.ITM.BeamRadius = 0.00303;	% 1/e^2 power radius, m
  ifo.Optics.ITM.CoatingAbsorption = 0.5e-6; % absorption of ITM
  ifo.Optics.ITM.Transmittance  = 0.01;% ITM power transmittance [Power]
  ifo.Optics.SRM.Transmittance  = 0.07;	% SRM power transmittance [Power]
  ifo.Optics.SRC.Loss           = 0.001;	% SR mirror loss [Power]
  ifo.Optics.PhotoDetectorEfficiency = .01;% photo-detector quantum efficiency [nominal .9]

  % ajw 40m; don't know quadrature.dc yet!!!
  ifo.Optics.SRM.Tunephase = -1.5298;	% SRM tuning for 1500 Hz peak
  ifo.Optics.Quadrature.dc = pi/3;      % demod/detection/homodyne phase
  ifo.Optics.SRM.Tunephase = 0.7447;	% SRM tuning for 4000 Hz peak

  ifo.Optics.Quadrature.dc = pi/2;     % demod/detection/homodyne phase
  ifo.Optics.ETM.BeamRadius = 0.00524;	% 1/e^2 power radius, m
  
  ifo.Laser.Power      = 1;		% W

  % Residual gas for 40m:
  ifo.ResidGas(1).Pressure = 1.e-8; % H2, torr
  ifo.ResidGas(2).Pressure = 5.e-7; % H2O, torr
  ifo.ResidGas(3).Pressure = 3.e-7; % N2, torr
end

%%%%%

% Single-pendulum suspensions:
if (iifo == 1)
  ifo.Suspension.fPend = 1;
  % Code for IFOModel:
  ifo.Materials.MirrorMass = 10.8;   % kg
  ifo.Suspension.fPend   = 0.744;    % Pendulum frequency, Hz
  ifo.Suspension.phiPend = 1.7e-4;    % pendulum loss angle, radians  
  ifo.Suspension.Nwires = 2;         % number of wires (2 per loop)
  ifo.Suspension.rhoWire = 0.588e-3; % linear mass density of wire, kg/m (310um diameter steel)
  % NOTE: specifying violin modes may make the benchmark code not converge.
  %  This should be non-zero only for noise plots:
  ifo.Suspension.Nviolin = 0;        % number of violin modes to include

elseif (iifo == 4)
  % 40m parameters:
  ifo.Materials.MirrorMass2 = 1.3;    % kg
  ifo.Materials.MirrorQ     = 1e6;    % effective on-resonance Q
  ifo.Suspension.fPend   = 0.800;     % Pendulum frequency, Hz
  ifo.Suspension.phiPend = 3e-4;    % pendulum loss angle, radians  
  ifo.Suspension.Nwires = 2;          % number of wires (2 per loop)
  ifo.Suspension.rhoWire = 0.052e-3;  % linear mass density of wire, kg/m (92 um dia steel)
  ifo.Suspension.Nviolin = 2;         % number of violin modes to include
end

%%%%%%

return
