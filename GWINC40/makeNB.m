% 40m Bench plots using the AdvLIGO Bench code
% 
% v1.0    Inserted into the ISC Modeling / Bench CVS tree   070930  Rana
% v2.0    Converted to use GWINC and PendThermTE for SUS therm

%%
global ifo

% must have a local checkout of gwinc from the iscmodeling SVN
% and set ISC_DIR as an environment variable
boo = getenv('ISC_DIR');
addpath([boo '/gwinc'])

f_seis = 29;
f_Nyq  = 6000;

P = 1;
SRCphase = 188;
T_SRM = 0.10;
T_ITM = 0.014;

ifo = IFOModel;
ifo = Mod40(ifo);

ifo.Optics.Quadrature.dc = 111 * pi/180;

figure(299792458)
[~, nnn] = gwinc40(f_seis, f_Nyq, ifo, [], 1,...
                   P, SRCphase*pi/180, T_SRM, T_ITM);

              
%% Plot modifications
goo = ifo.Infrastructure.Length;
%goo = 1;

set(gca,'FontSize',22)
xlabel('Frequency [Hz]',...
       'FontSize',22, 'Interpreter','Latex');

ylabel('Displacement [m/$\sqrt{\rm Hz}$]',...
       'FontSize',22, 'Interpreter','Latex');
title(['40m Displacement Noise w/ SOSs (P = ' num2str(P) ' W)'],...
       'Interpreter','Latex')
grid on
grid minor
axis([f_seis f_Nyq 1e-22*goo 1e-18*goo])


orient landscape

save 40mGwincNoise nnn





