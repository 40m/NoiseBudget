# Caltech 40m Noise Budget

Usage:

     python C1NB.py <params file>

Example parameter file: C1NB_2017-10-09.py

The noises:
- [x] Estimated freerunning MICH
- [x]Shot noise
- [x]Suspension thermal noise
- [x]Coating Brownian noise
- [x]Seismic noise
- [x]AS55 RFPD dark noise
- [ ]Laser Frequency noise
- [ ]Laser Intensity noise
- [ ]Oscillator Intensity noise
- [x]PRCL-> MICH control noise
- [x]SRCL-> MICH control noise
- [x]Suspension DAC and driver noises
- [x]Oplev A2L 
- [x]Residual gas
