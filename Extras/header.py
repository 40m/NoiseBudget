from __future__ import division

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import scipy.constants as scc
import scipy.optimize as opt
import scipy.integrate as scint
import scipy.interpolate as sctrp
import scipy.io as scio
import scipy.signal as sig
import scipy.special as scsp
#import sympy as s
import sys
import time
import uncertainties as unc

from uncertainties import ufloat as uf
from uncertainties import unumpy as unp

import nbutils as nbu

from gwpy.astro import inspiral_range
from gwpy.timeseries import TimeSeries, TimeSeriesDict
from IPython.display import Image, clear_output

# List of non-awful colors
cList = [
         (0, 0, 0.9, 0.8),
         (0.9, 0, 0, 0.8),
         (0.1, 0.7, 0.1, 0.8),
         (1.0, 0.2, 0.7, 0.8),
         (0.8, 0.8, 0, 0.8),
         (0, 0.5, 0.9, 0.8),
         (1, 0.5, 0, 0.8),
         (0.5, 0.5, 0.5, 0.8),
         (0.4, 0, 0.4, 0.8),
         (0, 0, 0, 0.8),
         (0.5, 0.2, 0, 0.8),
         (0, 0.3, 0, 0.8),
         (0, 0.7, 0.4, 0.8),
         (0.7, 0, 0, 0.8),
        ]

# Undo gwpy's craven attempt to alter my matplotlib parameters
mpl.rc_file_defaults()

# Now alter my matplotlib parameters
mpl.rcParams.update({'axes.color_cycle': cList,
                     'axes.grid': True,
                     'axes.grid.which': 'both',
                     'figure.figsize': (4, 3),
                     'font.family': 'serif',
                     'font.size': 10,
                     'grid.alpha': 0.3,
                     'grid.color': (0.5, 0.5, 0.5),
                     'grid.linestyle': 'solid',
                     'grid.linewidth': 0.8,
                     'legend.borderpad': 0.1,
                     'legend.fancybox': True,
                     'legend.fontsize': 11,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.1,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'lines.linewidth': 1.5,
                     'savefig.transparent': True,
                     'savefig.bbox': 'tight',
                     'savefig.pad_inches': 0.03,
                     'text.usetex': True,
                     'text.latex.preamble': r'\usepackage{amsmath},' \
                          r'\usepackage{fouriernc},' \
                          r'\usepackage[scale=0.96]{tgschola}'
                     })

# Bbox dict for text on plots
text_bbox = {
             'fc': (1,1,1),
             'alpha': 0.7,
             'ec': (1,1,1),
            }
