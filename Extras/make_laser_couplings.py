from header import *

rin_dcpd_date = '2015-08-01'
f_rin_dcpd, rin_dcpd_tf_re, rin_dcpd_tf_im = np.loadtxt('../Data/Intensity/IntensityCoupling_{}_tf.txt'.format(rin_dcpd_date), unpack=1)
rin_dcpd_tf = rin_dcpd_tf_re + 1j*rin_dcpd_tf_im
_, rin_dcpd_coh = np.loadtxt('../Data/Intensity/IntensityCoupling_{}_coh.txt'.format(rin_dcpd_date), unpack=1)
f_rin_dcpd = f_rin_dcpd[rin_dcpd_coh>0.9]
rin_dcpd_tf = rin_dcpd_tf[rin_dcpd_coh>0.9]
rin_dcpd_tf /= nbu.aa_16k(f_rin_dcpd)
rin_dcpd_tf /= 20 # mA to RIN

hh_rin, axm_rin, axp_rin = nbu.bode_plot(f_rin_dcpd, rin_dcpd_tf)
hh_rin.set_size_inches(4,5)
plt.setp(axm_rin.lines, marker='o')
plt.setp(axp_rin.lines, marker='o')
hh_rin.savefig('../Figures/Intensity/darm_rin_coupling_tf.pdf')
