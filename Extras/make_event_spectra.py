from header import *

gw150914_start = '2015-09-14 09:45:45'
gw150914_stop = '2015-09-14 09:55:45'
lvt151012_start = '2015-10-12 09:49:43'
lvt151012_stop = '2015-10-12 09:59:43'
gw151226_start = '2015-12-26 03:33:53'
gw151226_stop = '2015-12-26 03:43:53'

channel = 'H1:CAL-DELTAL_EXTERNAL_DQ'
ff_cal = np.logspace(-1, 4, 1000)
cal = np.abs((1+1j*ff_cal/100)/(1+1j*ff_cal/1))**5

ff, gw150914_disp = nbu.get_ifo_data(channel, gw150914_start, gw150914_stop,
        '../Data/DARM/', aa='16k', binNum=1000, calibration=(ff_cal, cal))
ff, lvt151012_disp = nbu.get_ifo_data(channel, lvt151012_start, lvt151012_stop,
        '../Data/DARM/', aa='16k', binNum=1000, calibration=(ff_cal, cal))
ff, gw151226_disp = nbu.get_ifo_data(channel, gw151226_start, gw151226_stop,
        '../Data/DARM/', aa='16k', binNum=1000, calibration=(ff_cal, cal))

hh, ax = plt.subplots()
hh.set_size_inches(5, 4)
ax.loglog(ff, gw150914_disp/4e3, label='2015--09--14', lw=1)
ax.loglog(ff, lvt151012_disp/4e3, label='2015--10--12', lw=1)
ax.loglog(ff, gw151226_disp/4e3, label='2015--12--26', lw=1)
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel(r'ASD of strain $\bigl[1/\text{Hz}^{1/2}\bigr]$')
ax.set_xlim(9, 5e3)
ax.set_ylim(3e-24, 3e-20)
ax.legend()
hh.savefig('../Figures/DARM/event_spectra.pdf')
