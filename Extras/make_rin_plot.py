from header import *

ff, rin_il, rin_ool = np.loadtxt('../Data/Intensity/rin_outerloop_2015-10-24_133000.txt', unpack=1)

hh, ax = plt.subplots()
ax.loglog(ff, rin_il, label='In-loop')
ax.loglog(ff, rin_ool, label='Out-of-loop')
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel(r'ASD of relative intensity noise $\bigl[1/\text{Hz}^{1/2}\bigr]$')
ax.set_xlim([0.3, 5e3])
ax.set_ylim([9e-11, 2.5e-5])
ax.legend()
hh.savefig('../Figures/Intensity/rin_outerloop.pdf')
