from gwpy.timeseries import TimeSeries, TimeSeriesDict
import numpy as np
import matplotlib.pyplot as plt

chan_list = [
             'ASC-X_TR_B_NSUM_OUT16',
             'ASC-Y_TR_B_NSUM_OUT16',
             'LSC-POP_A_LF_OUT16',
             'LSC-REFL_A_LF_OUT16',
             ]
chan_list = ['H1:' + chan for chan in chan_list]

start = '2015-11-26 09:03:00'
stop  = '2015-11-26 09:25:00'

ts_dict = TimeSeriesDict.fetch(chan_list, start, stop,
            host='nds.ligo-wa.caltech.edu', port=31200)

tt = ts_dict[chan_list[0]].times.value
tt -= tt[0]
xtr = ts_dict[chan_list[0]].value
ytr = ts_dict[chan_list[1]].value
pop = ts_dict[chan_list[2]].value
refl = ts_dict[chan_list[3]].value

hh = plt.figure(figsize=(5,5))
ax1 = hh.add_subplot(211)
ax2 = hh.add_subplot(212, sharex=ax1)
ax1.semilogy(tt/60., xtr, label='X arm')
ax1.semilogy(tt/60., ytr, label='Y arm')
ax1.semilogy(tt/60., pop, label='Recycling')
ax1.grid()
ax2.plot(tt/60., refl)
ax2.grid()
ax2.set_xlabel('Time [min.]')
hh.tight_layout()
hh.savefig('Figures/LockAcq.pdf')
