disp('OMG! Do not use hard path links')
disp(' ')
disp('OMG! Save data as a mat file, not as terrible ascii....')

addpath(genpath('/ligo/svncommon/IscSVN/iscmodeling/trunk/gwinc'));

[score, noise] = gwinc(1, 8000, IFOModelO1, SourceModel, 2)

dlmwrite('Data/GwincCurves/quantumNoise.txt', [noise.Freq; sqrt(noise.Quantum)*4e3].', ...
        'delimiter', '\t')
dlmwrite('Data/GwincCurves/mirrorThermalNoise.txt', [noise.Freq; sqrt(noise.MirrorThermal.Total)*4e3].', ...
        'delimiter', '\t')
dlmwrite('Data/GwincCurves/seismicNoise.txt', [noise.Freq; sqrt(noise.Seismic)*4e3].', ...
        'delimiter', '\t')
dlmwrite('Data/GwincCurves/newtonianNoise.txt', [noise.Freq; sqrt(noise.Newtonian)*4e3].', ...
        'delimiter', '\t')
dlmwrite('Data/GwincCurves/resGasNoise.txt', [noise.Freq; sqrt(noise.ResGas)*4e3].', ...
        'delimiter', '\t')
