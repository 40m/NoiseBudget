from header import *
import H1Params as p

#######################################
## dHard
#######################################

def make_asc_plot(dof, navg, xlim, ylim):

    print('Making {} OLTF plot...'.format(dof))
    ff_dof_p, dof_p_re, dof_p_im = np.loadtxt('Data/ASC/OLTFs/{}_PIT_20W_tf.txt'.format(dof.upper()), unpack=1)
    _, dof_p_coh = np.loadtxt('Data/ASC/OLTFs/{}_PIT_20W_coh.txt'.format(dof.upper()), unpack=1)
    dof_p_navg = navg
    dof_p_frunc = np.sqrt((1-dof_p_coh)/(2*dof_p_navg*dof_p_coh))
    ff_dof_y, dof_y_re, dof_y_im = np.loadtxt('Data/ASC/OLTFs/{}_YAW_20W_tf.txt'.format(dof.upper()), unpack=1)
    _, dof_y_coh = np.loadtxt('Data/ASC/OLTFs/{}_YAW_20W_coh.txt'.format(dof.upper()), unpack=1)
    dof_y_navg = navg
    dof_y_frunc = np.sqrt((1-dof_y_coh)/(2*dof_y_navg*dof_y_coh))

    dof_p_cohth = 0.7
    dof_p_mask = dof_p_coh>dof_p_cohth
    dof_y_cohth = 0.7
    dof_y_mask = dof_y_coh>dof_y_cohth

    dof_p_oltf = dof_p_re + 1j*dof_p_im
    dof_p_oltf = dof_p_oltf[dof_p_mask]
    dof_p_frunc = dof_p_frunc[dof_p_mask]
    ff_dof_p = ff_dof_p[dof_p_mask]
    dof_y_oltf = dof_y_re + 1j*dof_y_im
    dof_y_oltf = dof_y_oltf[dof_y_mask]
    dof_y_frunc = dof_y_frunc[dof_y_mask]
    ff_dof_y = ff_dof_y[dof_y_mask]

    h = plt.figure(figsize=(4,5))
    ax_mag = h.add_subplot(211)
    ax_pha = h.add_subplot(212, sharex=ax_mag)
    ax_mag.set_xscale('log', nonposx='clip')
    ax_mag.set_yscale('log', nonposx='clip')

    msx = 4
    ax_mag.errorbar(ff_dof_p, np.abs(dof_p_oltf), dof_p_frunc*np.abs(dof_p_oltf), fmt='o', ms=msx, label='Pitch')
    ax_pha.errorbar(ff_dof_p, np.angle(dof_p_oltf, deg=True), dof_p_frunc*180/np.pi, fmt='o', ms=msx)
    ax_mag.errorbar(ff_dof_y, np.abs(dof_y_oltf), dof_y_frunc*np.abs(dof_y_oltf), fmt='o', ms=msx, label='Yaw')
    ax_pha.errorbar(ff_dof_y, np.angle(dof_y_oltf, deg=True), dof_y_frunc*180/np.pi, fmt='o', ms=msx)

    nbu.set_grid(ax_mag)
    nbu.set_grid(ax_pha)
    ax_mag.set_ylabel(r'Magnitude [rad/rad]')
    ax_mag.set_ylim(*ylim)
    ax_mag.legend(numpoints=1)
    ax_pha.set_xlabel(r'Frequency [Hz]')
    ax_pha.set_ylabel(r'Phase')
    myyticks = np.arange(-225, 226, 45)
    myyticklabels = ['$'+str(tick)+'^\circ$' for tick in myyticks]
    ax_pha.set_yticks(myyticks)
    ax_pha.set_yticklabels(myyticklabels)
    ax_pha.set_xlim(*xlim)
    ax_pha.set_ylim(-190, 190)

    h.tight_layout()
    h.savefig('Figures/ASC/{}_oltf.pdf'.format(dof))

make_asc_plot('dhard', 5, [0.45, 8], [0.09, 25])
make_asc_plot('mich', 3, [0.5, 22], [0.09, 25])
make_asc_plot('src2', 3, [0.3, 5], [0.09, 25])
