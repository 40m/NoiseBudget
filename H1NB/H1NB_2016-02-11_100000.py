from header import *

nds_server = 'nds.ligo-wa.caltech.edu'
nds_port = 31200

darm_date = '2016-02-11'
darm_start = ' '.join([darm_date, '10:00:00'])
darm_stop = ' '.join([darm_date, '10:30:00'])
darm_oltf_path = 'Data/Calibration/2015-10-15_before_flip_tf.txt'

opt_gain_ref = 3.26e12 # mA/m
darm_pole_ref = 341 # Hz

f_calibration, dcpd_calibration_mag, dcpd_calibration_pha = \
        np.loadtxt('Data/OMC/dcpd_16kHz_calibration.txt', unpack=1)
dcpd_calibration_mag = 10**(dcpd_calibration_mag/20)

wavelength = 1064e-9 # m
power = 23*0.88 # W
arm_length = 4e3 # m
mass = 40 # kg
arm_trans = 0.0146
srm_trans = 0.37

esd_bias = 380 # V
esd_coeff = 2e-10 # N/V**2

dac_noise = lambda ff: 300e-9*np.sqrt(1+(50./ff)**2)

#######################################
# One-off functions
#######################################

def get_rad_press(ff, wavelength, power, mass, arm_length,
        arm_trans, srm_trans):
    Omega = 2*np.pi*ff
    gamma = arm_trans*scc.c/(4*arm_length)
    beta = np.arctan(Omega/gamma)
    rho = np.sqrt(1-srm_trans)
    tau = np.sqrt(srm_trans)
    Kappa = 8*power*(2*np.pi*scc.c/wavelength)/(mass*arm_length**2) \
            / (Omega**2*(gamma**2+Omega**2))
    KappaBar = Kappa*tau/(1+rho**2+2*rho*np.cos(2*beta))
    Ssql = 8*scc.Planck/(mass*Omega**2)
    Srp = Ssql/2*KappaBar
    return ff, np.sqrt(2*np.pi*Srp)

def get_shot(ff, dc_sum, opt_gain, darm_pole):
    return ff, np.ones(len(ff))*np.sqrt(2*scc.e*dc_sum*1e-3)*1e3

def get_dark_noise(fname):
    ff, dcpd_a, dcpd_b = np.loadtxt(fname, unpack=1)
    return ff, np.sqrt(dcpd_a**2 + dcpd_b**2)

def get_quad_tst_act(ff):
    zeros = [50, 50, 3250]
    poles = [2.2, 2.2, 152]
    gain = 2 * (2*esd_coeff*esd_bias)
    _, driver_tf = nbu.freqresp((zeros, poles, gain), ff)
    sus_resp = nbu.get_complex_interp('Data/SUS/quadTstTstLL.txt', ff)
    return ff, np.abs(driver_tf * sus_resp) * dac_noise(ff)

def get_quad_bias_act(ff):
    zeros = []
    poles = [1.8, 1.8, 1590]
    gain = 40 * (2*esd_coeff*esd_bias)
    _, driver_tf = nbu.freqresp((zeros, poles, gain), ff)
    sus_resp = nbu.get_complex_interp('Data/SUS/quadTstTstLL.txt', ff)
    return ff, np.abs(driver_tf * sus_resp) * dac_noise(ff)

def get_quad_pum_act(ff):
    # T1100378
    zeros = [6, 20, 1.35]
    poles = [0.5, 250, 110]
    gain = 0.268e-3*0.0309
    _, driver_tf = nbu.freqresp((zeros, poles, gain), ff)
    sus_resp = nbu.get_complex_interp('Data/SUS/quadPumTstLL.txt', ff)
    return ff, 2 * np.abs(driver_tf * sus_resp) * dac_noise(ff)

def get_bs_pum_act(ff):
    # T1100479
    zeros = [10, 20, 10]
    poles = [1, 200, 100]
    gain = 0.32e-3*0.963
    _, driver_tf = nbu.freqresp((zeros, poles, gain), ff)
    sus_resp = nbu.get_complex_interp('Data/SUS/bsfmM2M3LL.txt', ff)
    return ff, np.abs(driver_tf * sus_resp) * dac_noise(ff) / 280

def get_res_asd():
    ff, dcpd_sum = nbu.get_ifo_data('H1:OMC-DCPD_SUM_OUT_DQ', darm_start, darm_stop, 'OMC/')
    ff, dcpd_null = nbu.get_ifo_data('H1:OMC-DCPD_NULL_OUT_DQ', darm_start, darm_stop, 'OMC/')
    oltf = nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', ff)
    dcpd_null /= np.abs(1+oltf)
    return ff, np.sqrt(dcpd_sum**2 - dcpd_null**2)

def get_pd_bias():
    ff = np.logspace(0, 4, 200)
    reg_noise = 0.9e-6*(187./ff) # V/rtHz
    coupling = np.sqrt(2)*17e-3 # mA/V
    return ff, reg_noise*coupling


#######################################
# Noise dictionaries
#######################################

dcpd_sum_dict = {
    'label': r'Measured',
    'function': nbu.get_ifo_data,
    'args': ('H1:OMC-DCPD_SUM_OUT_DQ',
        darm_start,
        darm_stop,
        'OMC/',),
    'kwargs': {
        'binNum': 10000,
        'calibration': (f_calibration, dcpd_calibration_mag)},
    'type': 'sensing',
    }

dcpd_res_dict = {
    'label': r'Residual',
    'function': get_res_asd,
    'type': 'sensing',
    }

dark_dict = {
    'label': r'Dark noise',
    'function': get_dark_noise,
    'args': ('Data/OMC/dcpd_dark_wh1.txt',),
    'type': 'sensing',
    }

pd_bias_dict = {
    'label': r'PD bias noise',
    'function': get_pd_bias,
    'type': 'sensing',
    }

shot_dict = {
    'label': r'Shot noise',
    'group': r'Quantum noise',
    'function': get_shot,
    'args': (np.logspace(-1, 5, 300),
        20.0,
        3.24e12,
        339),
    'type': 'null',
    }

rad_press_dict = {
    'label': r'Radiation pressure',
    'group': r'Quantum noise',
    'function': get_rad_press,
    'args': (np.logspace(-1, 5, 300),
        wavelength, power, mass,
        arm_length, arm_trans, srm_trans),
    'type': 'displacement',
    }

seismic_dict = {
    'label': 'Seismic noise',
    'group': 'Seismic+Newtonian',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/seismicNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

newtonian_dict = {
    'label': 'Newtonian noise',
    'group': 'Seismic+Newtonian',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/newtonianNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

coat_brownian_dict = {
    'label': r'Coating Brownian noise',
    'group': r'Thermal',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/coatBrownianNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

sus_thermal_dict = {
    'label': r'Suspension thermal noise',
    'group': r'Thermal',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/susThermalNoise_Sheila.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

res_gas_dict = {
    'label': r'Residual gas noise',
    'group': r'Gas noise',
    'function': np.loadtxt,
    'args': ('Data/GwincCurves/resGasNoise.txt',),
    'kwargs': {'unpack': 1},
    'type': 'displacement',
    }

sfd_dict = {
    'type': 'displacement',
    'group': r'Gas noise',
    'label': r'Squeeze film damping',
    'function': np.loadtxt,
    'args': ('Data/SUS/squeeze_film_asd.txt',),
    'kwargs': {'unpack': 1},
    }

quad_tst_act_dict = {
    'type': 'displacement',
    'group': r'Actuator noise',
    'label': r'Quad test actuator noise',
    'function': get_quad_tst_act,
    'args': (np.logspace(-1, 5, 300),),
    }

quad_bias_act_dict = {
    'type': 'displacement',
    'group': r'Actuator noise',
    'label': r'Quad bias noise',
    'function': get_quad_bias_act,
    'args': (np.logspace(-1, 5, 300),),
    }

quad_pum_act_dict = {
    'type': 'displacement',
    'group': r'Actuator noise',
    'label': r'Quad PUM noise',
    'function': get_quad_pum_act,
    'args': (np.logspace(-1, 5, 300),),
    }

bs_pum_act_dict = {
    'type': 'displacement',
    'group': r'Actuator noise',
    'label': r'Beamsplitter PUM noise',
    'function': get_bs_pum_act,
    'args': (np.logspace(-1, 5, 300),),
    }

prcl_dict = {
        'label': r'PRCL',
        'group': r'LSC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:LSC-PRCL_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 04:31:30',
                 '2015-09-12 04:37:00',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'PRCL',
                 'DCPD',
                 'LSC/',
                 ),
        'kwargs': {'mask': (7, 400)},
        'type': 'sensing'
        }

mich_dict = {
        'label': r'MICH',
        'group': r'LSC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:LSC-MICH_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 04:23:00',
                 '2015-09-12 04:28:15',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'MICH',
                 'DCPD',
                 'LSC/',
                 ),
        'kwargs': {'mask': (7, 400)},
        'type': 'sensing'
        }

srcl_dict = {
        'label': r'SRCL',
        'group': r'LSC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:LSC-SRCL_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 04:15:20',
                 '2015-09-12 04:20:30',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'SRCL',
                 'DCPD',
                 'LSC/',
                 ),
        'kwargs': {'mask': (7, 400)},
        'type': 'sensing'
        }

dhard_p_dict = {
        'label': r'dHard pitch',
        'group': r'ASC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:ASC-DHARD_P_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 04:42:30',
                 '2015-09-12 04:47:30',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'dHard_P',
                 'DCPD',
                 'ASC/',
                 ),
        'kwargs': {'mask': (7, 100)},
        'type': 'sensing'
        }

dhard_y_dict = {
        'label': r'dHard yaw',
        'group': r'ASC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:ASC-DHARD_Y_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 04:49:00',
                 '2015-09-12 04:56:00',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'dHard_Y',
                 'DCPD',
                 'ASC/',
                 ),
        'kwargs': {'mask': (7, 100)},
        'type': 'sensing'
        }

mich_p_dict = {
        'label': r'MICH pitch',
        'group': r'ASC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:ASC-MICH_P_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 05:03:00',
                 '2015-09-12 05:08:00',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'MICH_P',
                 'DCPD',
                 'ASC/',
                 ),
        'kwargs': {'mask': (7, 100)},
        'type': 'sensing'
        }

mich_y_dict = {
        'label': r'MICH yaw',
        'group': r'ASC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:ASC-MICH_Y_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 05:10:30',
                 '2015-09-12 05:15:30',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'MICH_Y',
                 'DCPD',
                 'ASC/',
                 ),
        'kwargs': {'mask': (7, 100)},
        'type': 'sensing'
        }

sr2_p_dict = {
        'label': r'SR2 pitch',
        'group': r'ASC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:ASC-SRC2_P_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 05:19:00',
                 '2015-09-12 05:23:30',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'SR2_P',
                 'DCPD',
                 'ASC/',
                 ),
        'kwargs': {'mask': (7, 100)},
        'type': 'sensing'
        }

sr2_y_dict = {
        'label': r'SR2 yaw',
        'group': r'ASC',
        'function': nbu.get_noise_coupling,
        'args': ('H1:ASC-SRC2_Y_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 05:28:30',
                 '2015-09-12 05:33:20',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'SR2_Y',
                 'DCPD',
                 'ASC/',
                 ),
        'kwargs': {'mask': (7, 100)},
        'type': 'sensing',
        }

def get_intensity_noise(start, stop):
    '''
    One-off function to load measured TF and produce coupling measurement.
    '''
    rin_dcpd_date = '2015-08-01'
    f_rin_dcpd, rin_dcpd_tf_re, rin_dcpd_tf_im = np.loadtxt('Data/Intensity/IntensityCoupling_{}_tf.txt'.format(rin_dcpd_date), unpack=1)
    rin_dcpd_tf = rin_dcpd_tf_re + 1j*rin_dcpd_tf_im
    _, rin_dcpd_coh = np.loadtxt('Data/Intensity/IntensityCoupling_{}_coh.txt'.format(rin_dcpd_date), unpack=1)
    f_rin_dcpd = f_rin_dcpd[rin_dcpd_coh>0.9]
    rin_dcpd_tf = rin_dcpd_tf[rin_dcpd_coh>0.9]
    ff, rin_asd = nbu.get_ifo_data('H1:PSL-ISS_SECONDLOOP_SUM58_REL_OUT_DQ', start, stop,
            'Intensity/', aa='16k') 
    rin_coupling = rin_asd * np.interp(ff, f_rin_dcpd, np.abs(rin_dcpd_tf))
    return ff, rin_coupling

rin_dict = {
        'type': 'sensing',
        'label': r'Intensity',
        'function': get_intensity_noise,
        'args': (darm_start, darm_stop),
        }

freq_dict = {
        'type': 'sensing',
        'label': r'Frequency',
        'function': np.loadtxt,
        'args': ('Data/CARM/freq_dcpd_coup_asd.txt',),
        'kwargs': {'unpack': 1},
        }

imc_pzt_p_dict = {
        'label': r'IMC PZT pitch',
        'group': r'PSL Jitter',
        'function': nbu.get_noise_coupling,
        'args': ('H1:IMC-WFS_B_I_PIT_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 05:48:20',
                 '2015-09-12 05:53:20',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'IMC_PZT_P',
                 'DCPD',
                 'Intensity/',
                 ),
        'kwargs': {'mask': (50, 700)},
        'type': 'sensing',
        }

imc_pzt_y_dict = {
        'label': r'IMC PZT yaw',
        'group': r'PSL Jitter',
        'function': nbu.get_noise_coupling,
        'args': ('H1:IMC-WFS_A_I_YAW_OUT_DQ',
                 'H1:OMC-DCPD_SUM_OUT_DQ',
                 '2015-09-12 05:56:10',
                 '2015-09-12 06:01:10',
                 '2015-09-12 06:30:30',
                 '2015-09-12 07:00:00',
                 darm_start,
                 darm_stop,
                 'IMC_PZT_Y',
                 'DCPD',
                 'Intensity/',
                 ),
        'kwargs': {'mask': (50, 700)},
        'type': 'sensing',
        }

trace_dict_list = [
        dcpd_sum_dict,
        shot_dict,
        rad_press_dict,
        dark_dict,
        pd_bias_dict,
        seismic_dict,
        newtonian_dict,
        coat_brownian_dict,
        sus_thermal_dict,
        quad_tst_act_dict,
        quad_bias_act_dict,
        quad_pum_act_dict,
        bs_pum_act_dict,
        res_gas_dict,
        sfd_dict,
        prcl_dict,
        mich_dict,
        srcl_dict,
        dhard_p_dict,
        dhard_y_dict,
        mich_p_dict,
        mich_y_dict,
        sr2_p_dict,
        sr2_y_dict,
        rin_dict,
        freq_dict,
        imc_pzt_p_dict,
        imc_pzt_y_dict,
        #dcpd_res_dict,
        ]
