# Script for plotting loop transfer functions
# Usage: python H1Loops.py <ifo-params>

import importlib

import nbutils as nbu

from header import *


p = importlib.import_module(sys.argv[1].replace('.py',''))

pop_noise_start = '2016-01-16 02:43:30'
pop_noise_stop = '2016-01-16 02:52:30'

# Setup

ff = np.logspace(-2, np.log10(7444.), 10000)

######
# DARM
######

## Loop

#f_darm_oltf, darm_oltf_re, darm_oltf_im = np.loadtxt(, unpack=1)
#darm_oltf = darm_oltf_re+1j*darm_oltf_im
darm_oltf = nbu.get_complex_interp('Data/DARM/darm_oltf_model_2015-09-23.txt', ff)

f_darm, re_darm, im_darm = np.loadtxt('Data/DARM/darm_oltf_2015-09-23_tf.txt', unpack=1)
_, coh_darm = np.loadtxt('Data/DARM/darm_oltf_2015-09-23_coh.txt', unpack=1)
oltf_darm = re_darm+1j*im_darm
oltf_darm = oltf_darm[coh_darm>0.7]
f_darm = f_darm[coh_darm>0.7]
coh_darm = coh_darm[coh_darm>0.7]

## Noise

f_darm_err, darm_err_asd = nbu.get_ifo_data('H1:OMC-DCPD_SUM_OUT_DQ', pop_noise_start, pop_noise_stop,
        'LSC/', fftlength=100, binNum=10000, aa='16k')
f_darm_err = f_darm_err[2:]
darm_err_asd = darm_err_asd[2:]
darm_err_asd /= np.abs(p.opt_gain/(1+1j*f_darm_err/p.darm_pole))
darm_err_asd = np.interp(ff, f_darm_err, darm_err_asd)
darm_free_asd = darm_err_asd*np.abs(1-darm_oltf)
np.savetxt('Data/DARM/darm_free_asd_{}.txt'.format(pop_noise_start), np.c_[ff, darm_free_asd])
np.savetxt('Data/DARM/darm_res_asd_{}.txt'.format(pop_noise_start), np.c_[ff, darm_err_asd])

## DARM freerunning/residual noise

h_darm_supp, ax_darm_supp = plt.subplots()
h_darm_supp.set_size_inches(4, 5)
ax_darm_supp.loglog(ff, darm_free_asd, c=cList[6], label='Freerunning')
ax_darm_supp.loglog(ff[darm_err_asd>0], nbu.cum_rms(ff, darm_free_asd)[darm_err_asd>0], '--', c=cList[6])
ax_darm_supp.loglog(ff, darm_err_asd, label='Residual', c=cList[8])
ax_darm_supp.loglog(ff[darm_err_asd>0], nbu.cum_rms(ff, darm_err_asd)[darm_err_asd>0], '--',  c=cList[8])
ax_darm_supp.set_xlim(9e-3, 2000)
ax_darm_supp.set_ylim(8e-21, 3e-5)
ax_darm_supp.set_xlabel('Frequency [Hz]')
ax_darm_supp.set_ylabel(r'\textsc{asd} of dipslacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
ax_darm_supp.grid('off', which='minor', axis='y')
ax_darm_supp.tick_params(axis='y', which='minor', left='off', right='off')
ax_darm_supp.legend()
h_darm_supp.tight_layout()
h_darm_supp.savefig('Figures/DARM/DarmSupp.pdf')

######
# PRCL
######

## Loop

print('Computing PRCL [using {} W/m]'.format(p.prcl_opt_gain*p.prcl_fudge))
prcl_m3_act = p.hstsM3_elec_gain*p.hstsM3M3LL_mech_tf(ff)
prcl_m1_act = p.prcl_digi_offl_tf(ff)*p.hstsM1_elec_gain*p.hstsM1M3LL_mech_tf(ff)

prcl_oltf_blocks = [
        p.prcl_opt_tf(ff),
        p.pop9_demod*p.pop9_digi_gain,
        p.adc_gain,
        p.prcl_digi_tf(ff),
        p.prcl_delay_tf(ff),
        p.sus_dac_gain,
        prcl_m3_act+prcl_m1_act,
        p.prcl_fudge,
        ]

prcl_oltf = -1
for block in prcl_oltf_blocks:
    prcl_oltf *= block

f_prcl, re_prcl, im_prcl = np.loadtxt('Data/LSC/PrclTf_2015-01-14.txt', unpack=1)
_, coh_prcl = np.loadtxt('Data/LSC/PrclCoh_2015-01-14.txt', unpack=1)
oltf_prcl = re_prcl+1j*im_prcl
oltf_prcl = oltf_prcl[coh_prcl>0.7]
f_prcl = f_prcl[coh_prcl>0.7]
coh_prcl = coh_prcl[coh_prcl>0.7]

## Prcl NB

f_prcl_err, prcl_err_asd = nbu.get_ifo_data('H1:LSC-PRCL_IN1_DQ', pop_noise_start, pop_noise_stop,
        'LSC/', fftlength=100, binNum=10000, aa='16k')
f_prcl_err = f_prcl_err[2:]
prcl_err_asd = prcl_err_asd[2:]
prcl_err_asd = np.interp(ff, f_prcl_err, prcl_err_asd / 0.035, left=0, right=0) * p.input_power
prcl_err_asd /= p.adc_gain * 10**(30./20) * p.pop9_demod * p.prcl_opt_gain
prcl_free_asd = prcl_err_asd*np.abs(1-prcl_oltf)
np.savetxt('Data/LSC/prcl_free_asd_{}.txt'.format(pop_noise_start),
        np.c_[ff[prcl_err_asd>0], prcl_free_asd[prcl_err_asd>0]])
np.savetxt('Data/LSC/prcl_res_asd_{}.txt'.format(pop_noise_start),
        np.c_[ff[prcl_err_asd>0], prcl_err_asd[prcl_err_asd>0]])

## Prcl freerunning/residual noise

h_prcl_supp, ax_prcl_supp = plt.subplots()
h_prcl_supp.set_size_inches(4, 4)
ax_prcl_supp.loglog(ff, prcl_free_asd, c=cList[6], label='Freerunning')
ax_prcl_supp.loglog(ff[prcl_err_asd>0], nbu.cum_rms(ff, prcl_free_asd)[prcl_err_asd>0], '--', c=cList[6])
ax_prcl_supp.loglog(ff, prcl_err_asd, label='Residual', c=cList[8])
ax_prcl_supp.loglog(ff[prcl_err_asd>0], nbu.cum_rms(ff, prcl_err_asd)[prcl_err_asd>0], '--',  c=cList[8])
ax_prcl_supp.set_xlim(9e-3, 2000)
ax_prcl_supp.set_ylim(6e-18, 5e-5)
ax_prcl_supp.set_xlabel('Frequency [Hz]')
ax_prcl_supp.set_ylabel(r'\textsc{asd} of dipslacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
ax_prcl_supp.legend()
h_prcl_supp.tight_layout()
h_prcl_supp.savefig('Figures/LSC/PrclSupp.pdf')

######
# MICH
######

## Loop

print('Computing MICH [using {} W/m]'.format(p.mich_opt_gain*p.mich_fudge))
mich_m2_act = p.bsfmM2_elec_gain*p.bsfmM2M3LL_mech_tf(ff)
mich_m1_act = p.mich_digi_offl_tf(ff)*p.bsfmM1_elec_gain*p.bsfmM1M3LL_mech_tf(ff)
mich_oltf_blocks = [
        p.mich_opt_tf(ff),
        p.pop45_demod*p.pop45_digi_gain*p.mich_fudge,
        p.adc_gain,
        p.mich_digi_tf(ff),
        p.mich_delay_tf(ff),
        p.sus_dac_gain,
        mich_m2_act+mich_m1_act,
        ]

mich_oltf = -1
for block in mich_oltf_blocks:
    mich_oltf *= block

f_mich, re_mich, im_mich = np.loadtxt('Data/LSC/MichTf_2015-01-14.txt', unpack=1)
_, coh_mich = np.loadtxt('Data/LSC/MichCoh_2015-01-14.txt', unpack=1)
oltf_mich = re_mich+1j*im_mich
oltf_mich = oltf_mich[coh_mich>0.8]
f_mich = f_mich[coh_mich>0.8]
coh_mich = coh_mich[coh_mich>0.8]

### Mich NB

f_mich_err, mich_err_asd = nbu.get_ifo_data('H1:LSC-MICH_IN1_DQ', pop_noise_start, pop_noise_stop,
        'LSC/', fftlength=100, binNum=10000, aa='16k')
f_mich_err = f_mich_err[2:]
mich_err_asd = mich_err_asd[2:]
mich_err_asd = np.interp(ff, f_mich_err, mich_err_asd / 0.08, left=0, right=0) * p.input_power
mich_err_asd /= p.adc_gain * 10**(30./20) * p.pop45_demod * p.mich_opt_gain * 1.3
mich_free_asd = mich_err_asd*np.abs(1-mich_oltf)
np.savetxt('Data/LSC/mich_free_asd_{}.txt'.format(pop_noise_start),
        np.c_[ff[mich_err_asd>0], mich_free_asd[mich_err_asd>0]])
np.savetxt('Data/LSC/mich_res_asd_{}.txt'.format(pop_noise_start),
        np.c_[ff[mich_err_asd>0], mich_err_asd[mich_err_asd>0]])

pop45q_shot_asd = np.sqrt(4*scc.Planck*scc.c/p.wavelength*p.pop_dc)*np.ones(len(ff)) / p.mich_opt_gain

bs_pum_act = np.abs(p.sus_dac_noise(ff) * p.bsfmM2_elec_tf(ff) * p.bsfmM2M3LL_mech_tf(ff))

itm_cbr = 0.5e-20*(100./ff)**0.5

bs_sub_tr = np.sqrt(16*scc.Boltzmann*p.temperature**2/(4*np.pi**3*ff**2)
        * p.silica_ctr**2 * p.silica_cond * (p.bs_thick + 2*p.itm_thick + 2*p.cp_thick) / (p.silica_heat_cap**2 * p.bs_spot**4))

mich_sei_start = '2015-10-24 13:30:00'
mich_sei_stop= '2015-10-24 14:00:00'
f_mich_sei, bs_sp_seismic_x = nbu.get_ifo_data('H1:SUS-BS_M1_ISIINF_X_OUT_DQ', mich_sei_start, mich_sei_stop,
    'SUS/', fftlength=100, binNum=10000)
f_mich_sei, bs_sp_seismic_y = nbu.get_ifo_data('H1:SUS-BS_M1_ISIINF_Y_OUT_DQ', mich_sei_start, mich_sei_stop,
    'SUS/', fftlength=100, binNum=10000)
bs_sp_seismic = np.sqrt(bs_sp_seismic_x**2 + bs_sp_seismic_y**2)*1e-9
bs_sp_seismic = np.interp(ff, f_mich_sei, bs_sp_seismic, left=0, right=0)
bs_seismic = bs_sp_seismic * np.abs(p.bsfmGndM3LL_mech_tf(ff))

mich_noise_list = [
    (ff, mich_err_asd*np.abs(1-mich_oltf), 'Total'),
    (ff, pop45q_shot_asd, 'Shot noise'),
    (ff, bs_pum_act, r'\textsc{dac} noise'),
    (ff, bs_sub_tr, r'Substrate TR'),
    #(ff, itm_cbr, 'Test mass Brownian'),
    (ff, bs_seismic, 'Seismic'),
    ]


h_mich, ax_mich = plt.subplots()
for ff, asd, label in mich_noise_list:
    ax_mich.loglog(ff, asd, label=label)

ax_mich.minorticks_off()

ax_mich.set_xlim(0.025, 2e3)
ax_mich.set_ylim(3e-19, 1.4e-13)

ax_mich.set_xlabel('Frequency [Hz]')
ax_mich.set_ylabel(r'\textsc{asd} of displacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')

ax_mich.grid('off', which='minor')

ax_mich.legend()

h_mich.tight_layout()
h_mich.savefig('Figures/LSC/MichNB.pdf')

### Mich freerunning/residual

h_mich_supp, ax_mich_supp = plt.subplots()
h_mich_supp.set_size_inches(4, 4)
ax_mich_supp.loglog(ff, mich_err_asd*np.abs(1-mich_oltf), c=cList[6], label='Freerunning')
ax_mich_supp.loglog(ff[mich_err_asd>0], nbu.cum_rms(ff, mich_err_asd*np.abs(1-mich_oltf))[mich_err_asd>0], '--', c=cList[6])
ax_mich_supp.loglog(ff, mich_err_asd, label='Residual', c=cList[8])
ax_mich_supp.loglog(ff[mich_err_asd>0], nbu.cum_rms(ff, mich_err_asd)[mich_err_asd>0], '--',  c=cList[8])
ax_mich_supp.set_xlim(9e-3, 2e3)
ax_mich_supp.set_ylim(6e-18, 5e-5)
ax_mich_supp.set_xlabel('Frequency [Hz]')
ax_mich_supp.set_ylabel(r'\textsc{asd} of dipslacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
ax_mich_supp.legend()
h_mich_supp.tight_layout()
h_mich_supp.savefig('Figures/LSC/MichSupp.pdf')

######
# SRCL
######

print('Computing SRCL [using {} W/m]'.format(p.srcl_opt_gain*p.srcl_fudge))
srcl_m3_act = p.hstsM3_elec_gain*p.hstsM3M3LL_mech_tf(ff)
srcl_m1_act = p.srcl_digi_offl_tf(ff)*p.hstsM1_elec_gain*p.hstsM1M3LL_mech_tf(ff)
srcl_oltf_blocks = [
        p.srcl_opt_tf(ff),
        p.pop45_demod*p.pop45_digi_gain*p.srcl_fudge,
        p.adc_gain,
        p.srcl_digi_tf(ff),
        p.srcl_delay_tf(ff),
        p.sus_dac_gain,
        srcl_m3_act+srcl_m1_act,
        ]

srcl_oltf = -1
for block in srcl_oltf_blocks:
    srcl_oltf *= block

f_srcl, re_srcl, im_srcl = np.loadtxt('Data/LSC/SrclTf_2015-01-14.txt', unpack=1)
_, coh_srcl = np.loadtxt('Data/LSC/SrclCoh_2015-01-14.txt', unpack=1)
oltf_srcl = re_srcl+1j*im_srcl
oltf_srcl = oltf_srcl[coh_srcl>0.7]
f_srcl = f_srcl[coh_srcl>0.7]
coh_srcl = coh_srcl[coh_srcl>0.7]

### SRCL NB

f_srcl_err, srcl_err_asd = nbu.get_ifo_data('H1:LSC-SRCL_IN1_DQ', pop_noise_start, pop_noise_stop,
        'LSC/', fftlength=100, binNum=10000, aa='16k')
f_srcl_err = f_srcl_err[2:]
srcl_err_asd = srcl_err_asd[2:]
srcl_err_asd = np.interp(ff, f_srcl_err, srcl_err_asd / 0.08, left=0, right=0) * p.input_power
srcl_err_asd /= p.adc_gain * 10**(30./20) * p.pop45_demod * p.srcl_opt_gain
srcl_free_asd = srcl_err_asd*np.abs(1-srcl_oltf)
np.savetxt('Data/LSC/srcl_free_asd_{}.txt'.format(pop_noise_start),
        np.c_[ff[srcl_err_asd>0], srcl_free_asd[srcl_err_asd>0]])
np.savetxt('Data/LSC/srcl_res_asd_{}.txt'.format(pop_noise_start),
        np.c_[ff[srcl_err_asd>0], srcl_err_asd[srcl_err_asd>0]])

### SRCL freerunning/residual plot

h_srcl_supp, ax_srcl_supp = plt.subplots()
h_srcl_supp.set_size_inches(4, 4)
ax_srcl_supp.loglog(ff, srcl_err_asd*np.abs(1-srcl_oltf), c=cList[6], label='Freerunning')
ax_srcl_supp.loglog(ff[srcl_err_asd>0], nbu.cum_rms(ff, srcl_err_asd*np.abs(1-srcl_oltf))[srcl_err_asd>0], '--', c=cList[6])
ax_srcl_supp.loglog(ff, srcl_err_asd, label='Residual', c=cList[8])
ax_srcl_supp.loglog(ff[srcl_err_asd>0], nbu.cum_rms(ff, srcl_err_asd)[srcl_err_asd>0], '--',  c=cList[8])
ax_srcl_supp.set_xlim(9e-3, 2000)
ax_srcl_supp.set_ylim(6e-18, 5e-5)
ax_srcl_supp.set_xlabel('Frequency [Hz]')
ax_srcl_supp.set_ylabel(r'\textsc{asd} of dipslacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')
ax_srcl_supp.legend()
h_srcl_supp.tight_layout()
h_srcl_supp.savefig('Figures/LSC/SrclSupp.pdf')

####################
# Overall OLTF plots
####################

print('Plotting')
h_oltfs, axm_oltfs, axp_oltfs = nbu.bode_plot(ff, [prcl_oltf, mich_oltf, srcl_oltf])
axm_oltfs.set_ylabel('Magnitude [m/m]')
axm_oltfs.set_ylim(3e-4, 3e8)
axp_oltfs.set_xlim(0.03, 1.5e3)
axm_oltfs.grid('off', which='minor', axis='y')
axm_oltfs.legend([r'{\small PRC}', r'Michelson', r'{\small SRC}'])
axm_oltfs.tick_params(axis='y', which='minor', left='off', right='off')
h_oltfs.set_size_inches(4, 5)
h_oltfs.tight_layout(h_pad=0.12)
h_oltfs.savefig('Figures/LSC/OLTFs.pdf')

np.savetxt('Data/LSC/PrclTF_O1model.txt',
           np.c_[f_prcl, np.real(oltf_prcl), np.imag(oltf_prcl)])
np.savetxt('Data/LSC/MichTF_O1model.txt',
           np.c_[f_mich, np.real(oltf_mich), np.imag(oltf_mich)])
np.savetxt('Data/LSC/SrclTF_O1model.txt',
           np.c_[f_srcl, np.real(oltf_srcl), np.imag(oltf_srcl)])

axm_oltfs.loglog(f_prcl, np.abs(oltf_prcl), 'o', c=cList[0], alpha=0.4)
axm_oltfs.loglog(f_mich, np.abs(oltf_mich), 's', c=cList[1], alpha=0.4)
axm_oltfs.loglog(f_srcl, np.abs(oltf_srcl), 'D', c=cList[2], alpha=0.4)
axp_oltfs.semilogx(f_prcl, np.angle(oltf_prcl, deg=True), 'o', c=cList[0], alpha=0.4)
axp_oltfs.semilogx(f_mich, np.angle(oltf_mich, deg=True), 's', c=cList[1], alpha=0.4)
axp_oltfs.semilogx(f_srcl, np.angle(oltf_srcl, deg=True), 'D', c=cList[2], alpha=0.4)
axm_oltfs.set_ylim(3e-4, 200)
axp_oltfs.set_xlim(3, 1.5e3)
h_oltfs.savefig('Figures/LSC/OLTFs_zoom.pdf')

h_oltfs_xo, axm_oltfs_xo, axp_oltfs_xo = nbu.bode_plot(ff,
        [prcl_m1_act / prcl_m3_act, mich_m1_act / mich_m2_act, srcl_m1_act / srcl_m3_act])
axm_oltfs_xo.set_ylabel('Magnitude [m/m]')
axm_oltfs_xo.set_ylim(3e-4, 30)
axp_oltfs_xo.set_xlim(0.01, 30)
axm_oltfs_xo.grid('off', which='minor', axis='y')
axm_oltfs_xo.tick_params(axis='y', which='minor', left='off', right='off')
axm_oltfs_xo.legend([r'{\small PRC}', r'Michelson', r'{\small SRC}'])
h_oltfs_xo.set_size_inches(4, 5)
h_oltfs_xo.tight_layout()
h_oltfs_xo.savefig('Figures/LSC/XOs.pdf')

h_oltf_darm, axm_oltf_darm, axp_oltf_darm = nbu.bode_plot(ff, darm_oltf)
plt.setp(axm_oltf_darm.lines, c=(0.4, 0, 0.4))
plt.setp(axp_oltf_darm.lines, c=(0.4, 0, 0.4))
axm_oltf_darm.set_ylabel('Magnitude [m/m]')
axm_oltf_darm.set_ylim(3e-6, 5e8)
axp_oltf_darm.set_xlim(0.03, 5e3)
axm_oltf_darm.grid('off', which='minor', axis='y')
axm_oltf_darm.tick_params(axis='y', which='minor', left='off', right='off')
h_oltf_darm.set_size_inches(4, 5)
h_oltf_darm.tight_layout()
h_oltf_darm.savefig('Figures/DARM/DARM_OLTF.pdf')

axm_oltf_darm.loglog(f_darm, np.abs(oltf_darm), 'o',
        c=(0.5, 0, 0.5), alpha=0.4)
axp_oltf_darm.semilogx(f_darm, np.angle(oltf_darm, deg=True), 'o',
        c=(0.5, 0, 0.5), alpha=0.4)
axm_oltf_darm.set_ylim(3e-4, 500)
axp_oltf_darm.set_xlim(3, 1.5e3)
h_oltf_darm.savefig('Figures/DARM/DARM_OLTF_zoom.pdf')
