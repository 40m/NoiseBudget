from header import *

# General IFO params

wavelength = 1064e-9 # m
input_power = 23*0.88 # W
arm_length = 4e3 # m
prc_gain = 38

opt_gain = 3.2e12 # mA/m
darm_pole = 341 # Hz

mod_ind_9 = 0.2
mod_ind_45 = 0.28

darm_oltf = lambda ff: nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', ff)

# Optics params

## General

temperature = 300 # K

silica_cte = 5.1e-7 # K**-1
silica_ctr = 1.5e-5 # K**-1
silica_heat_cap = 1.64e6 # J/(m**3 K)
silica_cond = 1.38 # W/(m K)
silica_ref_ind = 1.46 # 

## Test masses

itm_trans = 0.0146

itm_thick = 0.2 # m

cp_thick = 0.1 # m

## Beamsplitter

bs_thick = 60e-3 # m
bs_spot = 53e-3 # m

## SR mirrors

srm_trans = 0.37


# Suspension params

quad_mass = 40 # kg

hstsM1M3LL_mech_tf = lambda ff: nbu.get_complex_interp('Data/SUS/hstsM1M3LL.txt', ff)
hstsM3M3LL_mech_tf = lambda ff: nbu.get_complex_interp('Data/SUS/hstsM3M3LL.txt', ff)

bsfmGndM3LL_mech_tf = lambda ff: nbu.get_complex_interp('Data/SUS/bsfmGndM3LL.txt', ff)
bsfmM1M3LL_mech_tf = lambda ff: nbu.get_complex_interp('Data/SUS/bsfmM1M3LL.txt', ff)
bsfmM2M3LL_mech_tf = lambda ff: nbu.get_complex_interp('Data/SUS/bsfmM2M3LL.txt', ff)


# Actuation electronics parameters

hstsM1_elec_gain = 11.9e-3 * 0.963 # N/V
hstsM1_elec_z = []
hstsM1_elec_p = []
hstsM1_elec_tf = lambda ff: 0

hstsM3_elec_gain = 2.8e-3 * 2.81e-3 # N/V
hstsM3_elec_z = [11, 21, 9]
hstsM3_elec_p = [1, 210, 82]
hstsM3_elec_tf = lambda ff: 0

bsfmM1_elec_gain = 11.9e-3 * 1.694 # N/V
bsfmM1_elec_z = []
bsfmM1_elec_p = []
bsfmM1_elec_tf = lambda ff: 0

bsfmM2_elec_gain = 0.32e-3 * 0.963 # N/V
bsfmM2_elec_z = [10, 20, 10]
bsfmM2_elec_p = [1, 200, 100]
# LP on, acq off...
bsfmM2_elec_tf = lambda ff: nbu.freqresp((bsfmM2_elec_z, bsfmM2_elec_p, bsfmM2_elec_gain), ff)[1]

sus_dac_gain = 20. / 2**18 # V/ct
sus_dac_noise = lambda ff: 300e-9*np.sqrt(1+(50./ff)**2)


# Sensing electronics parameters

adc_gain = 2**16 / 40. # ct/V
refl9_demod = 0 # V/W
pop9_demod = 4*1.7e6/adc_gain # V/W
pop45_demod = 4*1.8e6/adc_gain # V/W

pop_dc = 17e-3 # W

pop9_digi_gain = 4 * 10**(18./20) / input_power # ct/ct
pop45_digi_gain = 10**(30./20) / input_power # ct/ct


# Optical plant params

refl_power = 3.6e-3 # W
pop_power = 17e-3 # W

prcl_opt_coeff = 8e9*2*mod_ind_9/2 # W/m; see T1500461
prcl_opt_gain = prcl_opt_coeff*pop_power/(1*prc_gain)
prcl_opt_gain = 2.7e6
prcl_opt_tf = lambda ff: nbu.freqresp(([], [], prcl_opt_gain), ff)[1]

mich_opt_coeff = 7e8*2*mod_ind_45/2 # W/m; see T1500461
mich_opt_gain = mich_opt_coeff*pop_power/(1*prc_gain)
mich_opt_gain = 0.6e6
# W/m; see T1500461
mich_opt_tf = lambda ff: nbu.freqresp(([], [], mich_opt_gain), ff)[1]

srcl_opt_coeff = 1e8*2*mod_ind_45/2 # W/m; see T1500461
srcl_opt_gain = srcl_opt_coeff*pop_power/(1*prc_gain)  # W/m; see T1500461
srcl_opt_gain = 0.13e6
srcl_opt_tf = lambda ff: nbu.freqresp(([], [], srcl_opt_gain), ff)[1]

# Digital parameters

prcl_digi_gain = 8*0.035 # ct/ct
prcl_delay = 400e-6 # s
prcl_digi_tf = lambda ff: nbu.get_complex_interp('Data/LSC/LSC_PRCL_2_3_4_9_10.txt', ff) * prcl_digi_gain
prcl_delay_tf = lambda ff: np.exp(-2j*np.pi*ff*prcl_delay)
prcl_offl_gain = 0.02 # ct/ct
prcl_digi_offl_tf = lambda ff: nbu.get_complex_interp('Data/SUS/SUSPRM_M1_LOCK_L_1_2_4_6_8_9_10.txt', ff) * prcl_offl_gain

prcl_fudge = 1

mich_digi_gain = 2.9*0.08 # ct/ct
mich_delay = 400e-6 # s
mich_digi_tf = (lambda ff: nbu.get_complex_interp('Data/LSC/LSC_MICH_1_2_3_4_5_7_9.txt', ff)
                         * nbu.get_complex_interp('Data/SUS/SUSBS_M3_LOCK_L_4_5.txt', ff)
                         * nbu.get_complex_interp('Data/SUS/SUSBS_M2_LOCK_L_5_9_10.txt', ff) * mich_digi_gain)
mich_delay_tf = lambda ff: np.exp(-2j*np.pi*ff*mich_delay)
mich_offl_gain = 0.1 # ct/ct
mich_digi_offl_tf = lambda ff: nbu.get_complex_interp('Data/SUS/SUSBS_M1_LOCK_L_1_2.txt', ff) * mich_offl_gain

mich_fudge = 1

srcl_digi_gain = 16*0.08 # ct/ct
srcl_delay = 400e-6 # s
srcl_digi_tf = lambda ff: nbu.get_complex_interp('Data/LSC/LSC_SRCL_1_3_4_8_9_10.txt', ff) * srcl_digi_gain
srcl_delay_tf = lambda ff: np.exp(-2j*np.pi*ff*srcl_delay)
srcl_offl_gain = 0.02 # ct/ct
srcl_digi_offl_tf = lambda ff: nbu.get_complex_interp('Data/SUS/SUSSRM_M1_LOCK_L_1_2_4_6_8_10.txt', ff) * srcl_offl_gain

srcl_fudge = 1
