from header import *
import H1Params as p

ff, pop45q_asd = np.loadtxt('Data/LSC/pop45q_asd.txt', unpack=1)

pop45q_asd /= p.adc_gain * 10**(30./20) * p.pop45_demod * p.mich_opt_gain * 1.3

#ff = np.logspace(0, 3.3, 300)

# MICH

## Loop

mich_m2_act = p.bsfmM2_elec_gain*p.bsfmM2M3LL_mech_tf(ff)
mich_m1_act = p.mich_digi_offl_tf(ff)*p.bsfmM1_elec_gain*p.bsfmM1M3LL_mech_tf(ff)
mich_oltf_blocks = [
        p.mich_opt_tf(ff),
        p.pop45_demod*p.pop45_digi_gain*p.mich_fudge,
        p.adc_gain,
        p.mich_digi_tf(ff),
        p.mich_delay_tf(ff),
        p.sus_dac_gain,
        mich_m2_act+mich_m1_act,
        ]

mich_oltf = -1
for block in mich_oltf_blocks:
    mich_oltf *= block

## Noises

shot_asd = np.sqrt(4*scc.Planck*scc.c/p.wavelength*p.pop_dc)*np.ones(len(ff)) / p.mich_opt_gain

bs_pum_act = np.abs(p.sus_dac_noise(ff) * p.bsfmM2_elec_tf(ff) * p.bsfmM2M3LL_mech_tf(ff))

itm_cbr = 0.5e-20*(100./ff)**0.5

#mich_inj = np.abs(mich_oltf)*shot_asd

mich_list = [
    (ff, pop45q_asd*np.abs(1-mich_oltf), 'Total'),
    (ff, shot_asd, 'Shot noise'),
    (ff, bs_pum_act, 'BS DAC noise'),
    (ff, itm_cbr, 'ITM coating Brownian'),
    #(ff, mich_inj, '---'),
    ]

## Plots

h_mich, ax_mich = plt.subplots()
for ff, asd, label in mich_list:
    ax_mich.loglog(ff, asd, label=label)

ax_mich.set_xlim(3, 2e3)
ax_mich.set_ylim(3e-21, 1.4e-14)

ax_mich.set_xlabel('Frequency [Hz]')
ax_mich.set_ylabel(r'ASD of displacement $\bigl[\text{m/Hz}^{1/2}\bigr]$')

ax_mich.legend()

h_mich.tight_layout()
h_mich.savefig('Figures/LSC/MichNB.pdf')
