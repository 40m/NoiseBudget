#from header import *
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

from scipy import constants as scc

Q = 170
f0 = 3.3e3 # Hz
temp = 300 # K
mass = 0.1 # kg

ff = np.logspace(1, 4, 400) # Hz
ww = 2*np.pi*ff

w0 = 2*np.pi*f0
phi = 1/Q
k = mass*w0**2

b = 2*np.pi*mass*f0/Q

Sstruct = (4*scc.Boltzmann*temp*k*phi / ww
            / ((k-mass*ww**2)**2 + k**2*phi**2))
S34 = (4*scc.Boltzmann*temp*k*phi / ww
            / ((k-mass*ww**2)**2 + k**2*phi**2)
            * (f0/ff)**0.5)
Svisc = (4*scc.Boltzmann*temp*b
            / ((k-mass*ww**2)**2+b**2*ww**2))

np.savetxt('Data/SUS/srm_structural_peek.txt', np.c_[ff, np.sqrt(Sstruct)])
np.savetxt('Data/SUS/srm_34_peek.txt', np.c_[ff, np.sqrt(S34)])

hh = plt.figure(figsize=(6,5))
ax = hh.add_subplot(111)
ax.loglog(ff, np.sqrt(S34), c=(0.2, 0.6, 0.2, 0.9), label='$f^{-3/4}$', lw=3)
ax.loglog(ff, np.sqrt(Sstruct), c=(0.2, 0.2, 0.9, 0.9), label='Structural', lw=3)
ax.loglog(ff, np.sqrt(Svisc), c=(0.9, 0.2, 0.2, 0.9), label='Viscous', lw=3)
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('ASD of displacement [m/Hz$^{\mathregular{1/2}}$]')
ax.set_title('SRC noise from {} kHz mode (Q = {})'.format(f0/1e3, Q))
ax.grid('on', which='both', lw=0.5, ls='dotted', c=(0.5, 0.5, 0.5), alpha=0.5)
ax.grid(ls='solid', which='major')
ax.legend(fancybox=True, framealpha=0.7, loc='lower left')
hh.tight_layout()
hh.savefig('srm_thermal_peek.pdf')
