from header import *

def step_multi(tt, d0, d1, tau, steps):
    steps = np.concatenate([[0], steps, [tt[-1]]])
    nsteps = len(steps)
    starts = [d0, d1]*len(steps)
    starts = starts[:len(steps)]
    stops = [d1, d0]*len(steps)
    stops = stops[:len(steps)]
    signs = [1, -1]*len(steps)
    signs = signs[:len(steps)]
    yy = np.zeros(len(tt))+d0
    for ii in range(1, nsteps-1):
        mask = np.all([tt>steps[ii], tt<=steps[ii+1]], axis=0)
        yy[mask] = (stops[ii-1]-starts[ii-1])*(1-np.exp(-(tt[mask]-tt[mask][0])/tau))+starts[ii-1]
    return yy

def get_resp(loop, start, stop, d0, d1, tau, method='switch'):
    print(loop)
    if method=='switch':
        offset_chan = 'H1:ASC-{}_SWSTAT'.format(loop)
    elif method=='offset':
        offset_chan = 'H1:ASC-{}_OFFSET'.format(loop)
    err_chan = 'H1:ASC-{}_IN1_DQ'.format(loop)
    chan_list = [offset_chan, err_chan]

    ts_dict = TimeSeriesDict.fetch(chan_list, start, stop,
            host='nds.ligo-wa.caltech.edu', port=31200)

    offset_ts = ts_dict[offset_chan]
    err_ts = ts_dict[err_chan]

    offset_t = np.squeeze(offset_ts.times.value)
    offset_t -= offset_t[0]
    offset_arr = np.squeeze(offset_ts.value)

    err_t = np.squeeze(err_ts.times.value)
    err_t -= err_t[0]
    err_arr = np.squeeze(err_ts.value)

    step_times = np.squeeze(offset_t[:-1][np.argwhere(np.diff(offset_arr)!=0)])
    if isinstance(step_times, float):
        step_times = [step_times]

    #step_multi_c = lambda tt, d0, d1, tau: step_multi(tt, d0, d1, tau, step_times)
    step_multi_c = lambda tt, tau: step_multi(tt, d0, d1, tau, step_times)

    #q = opt.curve_fit(step_multi_c, err_t, err_arr, p0=[d0, d1, tau])
    q = opt.curve_fit(step_multi_c, err_t, err_arr, p0=[tau])
    print(q[0], q[1])

    h, ax = plt.subplots()
    h.set_size_inches(12,4)
    ax.plot(err_t, err_arr)
    ax.plot(err_t, step_multi_c(err_t, *q[0]))
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Error signal [ct]')
    h.tight_layout()
    h.savefig('Figures/ASC/{}_step_resp.pdf'.format(loop))

    return err_t, step_times, q


#fit = step_multi(err_t, d0, d1, 10, step_times)
#step_multi_2 = lambda tt, d0, d1, tau: step_multi(tt, d0, d1, tau, step_times)

#q = opt.curve_fit(step_multi_2, err_t, err_arr, p0=[d0, d1, 10])

inp1_p_date = '2016-01-30'
inp1_p_start = ' '.join([inp1_p_date, '23:19:30'])
inp1_p_stop = ' '.join([inp1_p_date, '23:40:00']) 
inp1_y_date = ''
inp1_y_start = ' '.join(['2016-01-30', '23:55:00'])
inp1_y_stop = ' '.join(['2016-01-31', '00:17:00']) 
prc1_p_date = '2016-01-31'
prc1_p_start = ' '.join([prc1_p_date, '00:32:50'])
prc1_p_stop = ' '.join([prc1_p_date, '00:58:00']) 
prc1_y_date = '2016-01-31'
prc1_y_start = ' '.join([prc1_y_date, '01:08:45'])
prc1_y_stop = ' '.join([prc1_y_date, '01:23:00']) 
prc2_p_date = '2016-02-02'
prc2_p_start = ' '.join([prc2_p_date, '08:59:30'])
prc2_p_stop = ' '.join([prc2_p_date, '09:20:30']) 
prc2_y_date = '2016-02-02'
prc2_y_start = ' '.join([prc2_y_date, '08:29:40'])
prc2_y_stop = ' '.join([prc2_y_date, '08:50:40']) 
src1_p_date = '2016-01-31'
src1_p_start = ' '.join([src1_p_date, '01:33:45'])
src1_p_stop = ' '.join([src1_p_date, '01:54:45']) 
src1_y_date = '2016-01-31'
src1_y_start = ' '.join([src1_y_date, '02:03:05'])
src1_y_stop = ' '.join([src1_y_date, '02:20:35']) 
#src2_p_date = '2016-02-03'
#src2_p_start = ' '.join([src1_p_date, '07:02:15'])
#src2_p_stop = ' '.join([src1_p_date, '07:08:00']) 

inp1_p_q = get_resp('INP1_P', inp1_p_start, inp1_p_stop, 0, -1000, 10)
inp1_y_q = get_resp('INP1_Y', inp1_y_start, inp1_y_stop, 0, -1000, 10)
prc1_p_q = get_resp('PRC1_P', prc1_p_start, prc1_p_stop, 0, -0.03, 40)
prc1_y_q = get_resp('PRC1_Y', prc1_y_start, prc1_y_stop, 0, -0.025, 10)
prc2_p_q = get_resp('PRC2_P', prc2_p_start, prc2_p_stop, 0, -800, 10)
prc2_y_q = get_resp('PRC2_Y', prc2_y_start, prc2_y_stop, 0, -700, 10)
src1_p_q = get_resp('SRC1_P', src1_p_start, src1_p_stop, 0, -300, 8)
src1_y_q = get_resp('SRC1_Y', src1_y_start, src1_y_stop, 0, -1000, 4)
#src2_p_q = get_resp('SRC2_P', src2_p_start, src2_p_stop, 0.1, -0.1, 1)
