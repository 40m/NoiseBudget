from header import *
import H1Params as p

print('Making DARM XO plot...')
ff_pum_xo, pum_xo_re, pum_xo_im = np.loadtxt('Data/DARM/etmy_pum_xo_tf.txt', unpack=1)
_, pum_xo_coh = np.loadtxt('Data/DARM/etmy_pum_xo_coh.txt', unpack=1)
pum_xo_navg = 20
pum_xo_frunc = np.sqrt((1-pum_xo_coh)/(2*pum_xo_navg*pum_xo_coh))
ff_uim_xo, uim_xo_re, uim_xo_im = np.loadtxt('Data/DARM/etmy_uim_xo_tf.txt', unpack=1)
_, uim_xo_coh = np.loadtxt('Data/DARM/etmy_uim_xo_coh.txt', unpack=1)
uim_xo_navg = 3
uim_xo_frunc = np.sqrt((1-uim_xo_coh)/(2*uim_xo_navg*uim_xo_coh))

pum_xo_cohth = 0.5
pum_xo_mask = pum_xo_coh>pum_xo_cohth
uim_xo_cohth = 0.7
uim_xo_mask = uim_xo_coh>uim_xo_cohth

pum_xo_oltf = pum_xo_re + 1j*pum_xo_im
pum_xo_oltf = pum_xo_oltf[pum_xo_mask]
pum_xo_frunc = pum_xo_frunc[pum_xo_mask]
ff_pum_xo = ff_pum_xo[pum_xo_mask]
uim_xo_oltf = uim_xo_re + 1j*uim_xo_im
uim_xo_oltf = uim_xo_oltf[uim_xo_mask]
uim_xo_frunc = uim_xo_frunc[uim_xo_mask]
ff_uim_xo = ff_uim_xo[uim_xo_mask]

h = plt.figure(figsize=(4,5))
ax_mag = h.add_subplot(211)
ax_pha = h.add_subplot(212, sharex=ax_mag)
ax_mag.set_xscale('log', nonposx='clip')
ax_mag.set_yscale('log', nonposx='clip')

msx = 4
ax_mag.errorbar(ff_pum_xo, np.abs(pum_xo_oltf), pum_xo_frunc*np.abs(pum_xo_oltf),
        fmt='o', ms=msx, label='Penultimate')
ax_pha.errorbar(ff_pum_xo, np.angle(pum_xo_oltf, deg=True), pum_xo_frunc*180/np.pi,
        fmt='o', ms=msx)
ax_mag.errorbar(ff_uim_xo, np.abs(uim_xo_oltf), uim_xo_frunc*np.abs(uim_xo_oltf),
        fmt='o', ms=msx, label='Upper intermediate')
ax_pha.errorbar(ff_uim_xo, np.angle(uim_xo_oltf, deg=True), uim_xo_frunc*180/np.pi,
        fmt='o', ms=msx)

nbu.set_grid(ax_mag)
nbu.set_grid(ax_pha)
ax_mag.set_ylabel(r'Magnitude [m/m]')
ax_mag.set_ylim(0.05, 15)
ax_mag.legend(numpoints=1)
ax_pha.set_xlabel(r'Frequency [Hz]')
ax_pha.set_ylabel(r'Phase')
myyticks = np.arange(-225, 226, 45)
myyticklabels = ['$'+str(tick)+'^\circ$' for tick in myyticks]
ax_pha.set_yticks(myyticks)
ax_pha.set_yticklabels(myyticklabels)
ax_pha.set_xlim(0.45, 250)
ax_pha.set_ylim(-190, 190)

h.tight_layout()
h.savefig('Figures/DARM/etmy_xo.pdf')
