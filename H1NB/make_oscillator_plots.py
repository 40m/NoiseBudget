from header import *
import H1Params as p

#######################################
## 45 MHz oscillator RAN
#######################################

print('Making 45 MHz oscillator RAN spectrum plot...')
ff_rfam_45, rfam_45 = np.loadtxt('Data/Oscillator/rf45_ool_iop_asd.txt', unpack=1)
h_rfam_45, ax_rfam_45 = plt.subplots()
ax_rfam_45.loglog(ff_rfam_45, rfam_45)
ax_rfam_45.set_xlim(3, 28e3)
ax_rfam_45.set_ylim(8e-10, 5e-8)
ax_rfam_45.set_xlabel(r'Frequency [Hz]')
ax_rfam_45.set_ylabel(r'\textsc{asd} of \textsc{ram} $\bigl[\text{Hz}^{-1/2}\bigr]$')
nbu.set_grid(ax_rfam_45)
h_rfam_45.tight_layout()
h_rfam_45.savefig('Figures/rfam_45_asd.pdf')


###################################
## Oscillator amplitude coupling TF
###################################

print('Making oscillator RAN coupling TF plot...')
ff45, re45, im45 = np.loadtxt('Data/Oscillator/rfam45_dcpd_tf_2016-02-09.txt', unpack=1)
_, coh45 = np.loadtxt('Data/Oscillator/rfam45_dcpd_coh_2016-02-09.txt', unpack=1)
ff9, re9, im9 = np.loadtxt('Data/Oscillator/rfam9_dcpd_tf_2016-02-09.txt', unpack=1)
_, coh9 = np.loadtxt('Data/Oscillator/rfam9_dcpd_coh_2016-02-09.txt', unpack=1)

cohth = 0.5

tf45 = re45 + 1j*im45
tf45 *= 1-p.darm_oltf(ff45)
tf45 = tf45[coh45>cohth] / 20
ff45 = ff45[coh45>cohth]
coh45 = coh45[coh45>cohth]
tf9 = re9 + 1j*im9
tf9 *= 1-p.darm_oltf(ff9)
tf9 = tf9[coh9>cohth] / 20
ff9 = ff9[coh9>cohth]
coh9 = coh9[coh9>cohth]

h = plt.figure(figsize=(4,5))
ax_mag = h.add_subplot(311)
ax_pha = h.add_subplot(312, sharex=ax_mag)
ax_coh = h.add_subplot(313, sharex=ax_mag)

ax_mag.loglog(ff9, np.abs(tf9), '.')
ax_pha.semilogx(ff9, np.angle(tf9, deg=True), '.')
ax_coh.semilogx(ff9, coh9, '.')
ax_mag.loglog(ff45, np.abs(tf45), '.')
ax_pha.semilogx(ff45, np.angle(tf45, deg=True), '.')
ax_coh.semilogx(ff45, coh45, '.')

nbu.set_grid(ax_mag)
nbu.set_grid(ax_pha)
nbu.set_grid(ax_coh)
ax_mag.set_ylabel(r'Magnitude [\textsc{rpn/ran}]')
ax_mag.set_ylim(7e-4, 0.6)
ax_mag.legend(['9\,MHz', '45\,MHz'], numpoints=1)
ax_pha.set_ylabel(r'Phase')
myyticks = np.arange(-225, 226, 45)
myyticklabels = ['$'+str(tick)+'^\circ$' for tick in myyticks]
ax_pha.set_yticks(myyticks)
ax_pha.set_yticklabels(myyticklabels)
ax_pha.set_ylim(-190, 190)
ax_coh.set_ylabel('Coherence')
ax_coh.set_xlabel('Frequency [Hz]')
ax_coh.set_ylim(0.48, 1.02)
ax_coh.set_xlim(9, 8e3)

h.tight_layout()
h.savefig('Figures/Oscillator/rfam_dcpd_tf.pdf')


#######################################
## 45 MHz oscillator phase coupling TF
#######################################

print('Making 45 MHz oscillator phase coupling TF plot...')
ff, re, im = np.loadtxt('Data/Oscillator/ifr_45MHz_phase_dcpd_tf_2016-02-01.txt', unpack=1)
_, coh = np.loadtxt('Data/Oscillator/ifr_45MHz_phase_dcpd_coh_2016-02-01.txt', unpack=1)

tf = re + 1j*im
tf *= 1-p.darm_oltf(ff)
#tf /= aa_16k(ff)**2
# 20 mA for DCPD dc sum current...
tf = tf[coh>0.5] / 20
ff = ff[coh>0.5]
coh = coh[coh>0.5]

h = plt.figure(figsize=(4,5))
ax_mag = h.add_subplot(311)
ax_pha = h.add_subplot(312, sharex=ax_mag)
ax_coh = h.add_subplot(313, sharex=ax_mag)

ax_mag.loglog(ff, np.abs(tf), '.')
ax_pha.semilogx(ff, np.angle(tf, deg=True), '.')
ax_coh.semilogx(ff, coh, '.')

nbu.set_grid(ax_mag)
nbu.set_grid(ax_pha)
nbu.set_grid(ax_coh)
ax_coh.text(20, 0.95, '60 avg.', color=(0.5, 0.5, 0.5), va='center', ha='center',
        bbox=text_bbox)
ax_mag.set_ylabel(r'Magnitude [\textsc{rpn}/Hz]')
ax_mag.set_ylim(7e-9, 5e-6)
ax_pha.set_ylabel(r'Phase')
myyticks = np.arange(-225, 226, 45)
myyticklabels = ['$'+str(tick)+'^\circ$' for tick in myyticks]
ax_pha.set_yticks(myyticks)
ax_pha.set_yticklabels(myyticklabels)
ax_pha.set_ylim(-190, 190)
ax_coh.set_ylabel('Coherence')
ax_coh.set_xlabel('Frequency [Hz]')
ax_coh.set_ylim(0.48, 1.02)
ax_coh.set_xlim(9, 8e3)

h.tight_layout()
h.savefig('Figures/Oscillator/rfpm_45_dcpd_tf.pdf')
