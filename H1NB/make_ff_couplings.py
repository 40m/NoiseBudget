from header import *
import H1Params as p

fp = 350 # Hz
opt_gain = 3.3e12 # mA/m
darm_plant = lambda ff: opt_gain/(1+1j*ff/fp)

# prcl

## No FF

no_ff, prcl_dcpd_no_ff_tf_re, prcl_dcpd_no_ff_tf_im = np.loadtxt('Data/LSC/prcl_dcpd_coupling_no_ff_tf.txt', unpack=1)
prcl_dcpd_no_ff_tf = prcl_dcpd_no_ff_tf_re + 1j*prcl_dcpd_no_ff_tf_im
no_ff, prcl_dcpd_no_ff_coh = np.loadtxt('Data/LSC/prcl_dcpd_coupling_no_ff_coh.txt', unpack=1)

prcl_dcpd_no_ff_tf = prcl_dcpd_no_ff_tf[prcl_dcpd_no_ff_coh > 0.5]
no_ff = no_ff[prcl_dcpd_no_ff_coh > 0.5]
prcl_dcpd_no_ff_coh = prcl_dcpd_no_ff_coh[prcl_dcpd_no_ff_coh > 0.5]

prcl_dcpd_no_ff_tf /= p.sus_dac_gain * p.hstsM3_elec_gain * p.hstsM3M3LL_mech_tf(no_ff)

prcl_dcpd_no_ff_tf *= 1 - nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', no_ff)
undo_daq_downsample = nbu.get_complex_interp('Data/CDS/downsample_16384_8x.txt', no_ff)
prcl_dcpd_no_ff_tf *= undo_daq_downsample

## Plot

h_prcl_dcpd, axm_prcl_dcpd, axp_prcl_dcpd = nbu.bode_plot(no_ff, prcl_dcpd_no_ff_tf/3.3e12*np.abs(1+1j*no_ff/fp))
h_prcl_dcpd.set_size_inches(4, 5)

plt.setp(axm_prcl_dcpd.lines, alpha=0.5, marker='o', mew=0, ms=4, lw=0)
plt.setp(axp_prcl_dcpd.lines, alpha=0.5, marker='o', mew=0, ms=4, lw=0)

axm_prcl_dcpd.set_ylabel('Magnitude [m/m]')
axm_prcl_dcpd.set_ylim(8e-7, 6e-3)
axp_prcl_dcpd.set_xlim(9, 1.2e3)

axm_prcl_dcpd.legend(['Without feedforward'], numpoints=1,
        loc='upper center')

h_prcl_dcpd.tight_layout()
h_prcl_dcpd.savefig('Figures/LSC/prcl_darm_coupling.pdf')

# Mich

## No FF

no_ff, mich_dcpd_no_ff_tf_re, mich_dcpd_no_ff_tf_im = np.loadtxt('Data/LSC/mich_dcpd_coupling_no_ff_tf.txt', unpack=1)
mich_dcpd_no_ff_tf = mich_dcpd_no_ff_tf_re + 1j*mich_dcpd_no_ff_tf_im
no_ff, mich_dcpd_no_ff_coh = np.loadtxt('Data/LSC/mich_dcpd_coupling_no_ff_coh.txt', unpack=1)

mich_dcpd_no_ff_tf = mich_dcpd_no_ff_tf[mich_dcpd_no_ff_coh > 0.5]
no_ff = no_ff[mich_dcpd_no_ff_coh > 0.5]
mich_dcpd_no_ff_coh = mich_dcpd_no_ff_coh[mich_dcpd_no_ff_coh > 0.5]

bs_m2_lock_l_filt = lambda no_ff: nbu.get_complex_interp('Data/SUS/SUSBS_M2_LOCK_L_5_9_10.txt', no_ff)
mich_dcpd_no_ff_tf /= bs_m2_lock_l_filt(no_ff) * p.sus_dac_gain * p.bsfmM2_elec_gain * p.bsfmM2M3LL_mech_tf(no_ff)

mich_dcpd_no_ff_tf *= 1 - nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', no_ff)
undo_daq_downsample = nbu.get_complex_interp('Data/CDS/downsample_16384_8x.txt', no_ff)
mich_dcpd_no_ff_tf *= undo_daq_downsample

## With FF

with_ff, mich_dcpd_with_ff_tf_re, mich_dcpd_with_ff_tf_im = np.loadtxt('Data/LSC/mich_dcpd_coupling_with_ff_tf.txt', unpack=1)
mich_dcpd_with_ff_tf = mich_dcpd_with_ff_tf_re + 1j*mich_dcpd_with_ff_tf_im
with_ff, mich_dcpd_with_ff_coh = np.loadtxt('Data/LSC/mich_dcpd_coupling_with_ff_coh.txt', unpack=1)

mich_dcpd_with_ff_tf = mich_dcpd_with_ff_tf[mich_dcpd_with_ff_coh > 0.5]
with_ff = with_ff[mich_dcpd_with_ff_coh > 0.5]
mich_dcpd_with_ff_coh = mich_dcpd_with_ff_coh[mich_dcpd_with_ff_coh > 0.5]

bs_m2_lock_l_filt = lambda with_ff: nbu.get_complex_interp('Data/SUS/SUSBS_M2_LOCK_L_5_9_10.txt', with_ff)
mich_dcpd_with_ff_tf /= bs_m2_lock_l_filt(with_ff) * p.sus_dac_gain * p.bsfmM2_elec_gain * p.bsfmM2M3LL_mech_tf(with_ff)

mich_dcpd_with_ff_tf *= 1 - nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', with_ff)
undo_daq_downsample = nbu.get_complex_interp('Data/CDS/downsample_16384_8x.txt', with_ff)
mich_dcpd_with_ff_tf *= undo_daq_downsample

## Plot

print('Plotting...')

h_mich_dcpd, axm_mich_dcpd, axp_mich_dcpd = nbu.bode_plot(no_ff, mich_dcpd_no_ff_tf/3.3e12*np.abs(1+1j*no_ff/fp))
h_mich_dcpd.set_size_inches(4, 5)

axm_mich_dcpd.loglog(with_ff, np.abs(mich_dcpd_with_ff_tf)/3.3e12*np.abs(1+1j*with_ff/fp))
axp_mich_dcpd.semilogx(with_ff, np.angle(mich_dcpd_with_ff_tf, deg=True))

plt.setp(axm_mich_dcpd.lines, marker='o', mew=0, ms=4, lw=0)
plt.setp(axp_mich_dcpd.lines, marker='o', mew=0, ms=4, lw=0)

axm_mich_dcpd.set_ylabel('Magnitude [m/m]')
axm_mich_dcpd.set_ylim(3e-5, 0.06)
axp_mich_dcpd.set_xlim(9, 1.2e3)

axm_mich_dcpd.legend(['Without feedforward', 'With feedforward'], numpoints=1,
        loc='upper center')

h_mich_dcpd.tight_layout()
h_mich_dcpd.savefig('Figures/LSC/mich_darm_coupling.pdf')

# SRCL

## OLTF

srcl_oltf = lambda ff: nbu.get_complex_interp('Data/LSC/SrclTF_O1model.txt', ff)

## No FF

no_ff, srcl_dcpd_no_ff_tf_re, srcl_dcpd_no_ff_tf_im = np.loadtxt('Data/LSC/srcl_dcpd_coupling_no_ff_tf.txt', unpack=1)
srcl_dcpd_no_ff_tf = srcl_dcpd_no_ff_tf_re + 1j*srcl_dcpd_no_ff_tf_im
no_ff, srcl_dcpd_no_ff_coh = np.loadtxt('Data/LSC/srcl_dcpd_coupling_no_ff_coh.txt', unpack=1)

srcl_dcpd_no_ff_tf = srcl_dcpd_no_ff_tf[srcl_dcpd_no_ff_coh > 0.5]
no_ff = no_ff[srcl_dcpd_no_ff_coh > 0.5]
srcl_dcpd_no_ff_coh = srcl_dcpd_no_ff_coh[srcl_dcpd_no_ff_coh > 0.5]

srcl_dcpd_no_ff_tf /= p.sus_dac_gain * p.hstsM3_elec_gain * p.hstsM3M3LL_mech_tf(no_ff)

srcl_dcpd_no_ff_tf *= 1 - nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', no_ff)
undo_daq_downsample = nbu.get_complex_interp('Data/CDS/downsample_16384_8x.txt', no_ff)
srcl_dcpd_no_ff_tf *= undo_daq_downsample
srcl_dcpd_no_ff_tf /= darm_plant(no_ff)

srcl_dcpd_no_ff_fit = ((1.5e-3*(10/no_ff)**2 - 2.2e-4*(no_ff/1e3))
                        * np.exp(-2j*np.pi*0.7e-3*no_ff))

## With FF

with_ff, srcl_dcpd_with_ff_tf_re, srcl_dcpd_with_ff_tf_im = np.loadtxt('Data/LSC/srcl_dcpd_coupling_with_ff_tf.txt', unpack=1)
srcl_dcpd_with_ff_tf = srcl_dcpd_with_ff_tf_re + 1j*srcl_dcpd_with_ff_tf_im
with_ff, srcl_dcpd_with_ff_coh = np.loadtxt('Data/LSC/srcl_dcpd_coupling_with_ff_coh.txt', unpack=1)

srcl_dcpd_with_ff_tf = srcl_dcpd_with_ff_tf[srcl_dcpd_with_ff_coh > 0.5]
with_ff = with_ff[srcl_dcpd_with_ff_coh > 0.5]
srcl_dcpd_with_ff_coh = srcl_dcpd_with_ff_coh[srcl_dcpd_with_ff_coh > 0.5]

srcl_dcpd_with_ff_tf /= p.sus_dac_gain * p.hstsM3_elec_gain * p.hstsM3M3LL_mech_tf(with_ff)

srcl_dcpd_with_ff_tf *= 1 - nbu.get_complex_interp('Data/DARM/darm_oltf_2015-09-10_tf.txt', with_ff)
undo_daq_downsample = nbu.get_complex_interp('Data/CDS/downsample_16384_8x.txt', with_ff)
srcl_dcpd_with_ff_tf *= undo_daq_downsample
srcl_dcpd_with_ff_tf /= darm_plant(with_ff)

srcl_dcpd_no_ff_interp = (np.interp(with_ff, no_ff, np.real(srcl_dcpd_no_ff_tf))
                          + 1j*np.interp(with_ff, no_ff, np.imag(srcl_dcpd_no_ff_tf)))


srcl_dcpd_disp_ff_tf = ((srcl_dcpd_with_ff_tf - srcl_dcpd_no_ff_interp) * srcl_oltf(with_ff)
                         + srcl_dcpd_no_ff_interp) / (1 - srcl_oltf(with_ff))

## Plot

h_srcl_dcpd, axm_srcl_dcpd, axp_srcl_dcpd = nbu.bode_plot(no_ff, srcl_dcpd_no_ff_tf)
h_srcl_dcpd.set_size_inches(4, 5)

axm_srcl_dcpd.loglog(with_ff, np.abs(srcl_dcpd_with_ff_tf))
axm_srcl_dcpd.loglog(with_ff, np.abs(srcl_dcpd_disp_ff_tf))
#axm_srcl_dcpd.loglog(no_ff, np.abs(srcl_dcpd_no_ff_fit))
axp_srcl_dcpd.semilogx(with_ff, np.angle(srcl_dcpd_with_ff_tf, deg=True))
axp_srcl_dcpd.semilogx(with_ff, np.angle(srcl_dcpd_disp_ff_tf, deg=True))
#axp_srcl_dcpd.semilogx(no_ff, np.angle(srcl_dcpd_no_ff_fit, deg=True))

plt.setp(axm_srcl_dcpd.lines, alpha=0.5, marker='o', mew=0, ms=4, lw=0)
plt.setp(axp_srcl_dcpd.lines, alpha=0.5, marker='o', mew=0, ms=4, lw=0)

axm_srcl_dcpd.set_ylabel('Magnitude [m/m]')
axm_srcl_dcpd.set_ylim(8e-7, 1.2e-2)
axp_srcl_dcpd.set_xlim(9, 1.2e3)

axm_srcl_dcpd.legend(['Without feedforward',
                      'With feedforward (control)',
                      'With feedforward (displacement)'], numpoints=1,
        loc='upper center')

h_srcl_dcpd.tight_layout()
h_srcl_dcpd.savefig('Figures/LSC/srcl_darm_coupling.pdf')
