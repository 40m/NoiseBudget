from header import *

ff, pda, pdb, pdsum, pdnull = np.loadtxt('Data/OMC/dcpd_a_b_sum_null_asd_2015-12-02.txt', unpack=1)

ffx, xre, xim = np.loadtxt('Data/OMC/dcpd_a_b_xcorr_2015-12-02.txt', unpack=1)
xcorr = xre+1j*xim

ftf, rtf, itf = np.loadtxt('Data/DARM/darm_oltf_2015-09-23_tf.txt', unpack=1)
rtf = np.interp(ff, ftf, rtf)
itf = np.interp(ff, ftf, itf)
tf = rtf+1j*itf

xcorr_free = 0.5*((0.5*np.abs(tf)**2-tf)*pda**2
                  +(0.5*np.abs(tf)**2-np.conj(tf))*pdb**2
                  +0.5*np.abs(tf)**2*(xcorr+np.conj(xcorr))
                  -(tf+np.conj(tf))*xcorr)+xcorr

og = 3.22e12
fp = 334
plant = og/(1+1j*ff/fp)/(1+1j*ff/5e3)

darmsum = pdsum*np.abs(1-tf)/np.abs(plant)
darmnull = pdnull/np.abs(plant)
darmres = np.sqrt(np.abs(darmsum**2-darmnull**2))
darmxcorr = 2*np.abs(xcorr_free)**0.5/np.abs(plant)
cbr = 1.1e-20*(100/ff)**0.5
myswhite = 8e-8/7/np.abs(plant)
mysf2 = 1e-20*(100/ff)**2

np.savetxt('Data/DARM/DARM_sum_2015-12-02.txt', np.c_[ff, darmsum])
np.savetxt('Data/DARM/DARM_res_2015-12-02.txt', np.c_[ff, darmres])
np.savetxt('Data/DARM/DARM_xcorr_2015-12-02.txt', np.c_[ff, darmxcorr])

plt.figure(figsize=(4,3))
plt.loglog(ff, darmnull, c='#332288', lw=2,
    label=r"Uncorrelated noise (,,null``)")
plt.loglog(ff, darmsum, c='#cc6677', lw=2,
    label=r"Total noise (,,sum``)")
#plt.loglog(ff, darmres, 'g', lw=2,
#    label=r'Residual')
plt.loglog(ff, darmxcorr, '#117733', lw=2,
    label=r'Photodiode cross-correlation')
plt.loglog(ff, cbr, c='#aa4499', lw=2,
    label=r'Expected coating thermal noise')
#plt.loglog(ff, 1.45*cbr, 'm--', lw=2,
#    label=r'Coating Brownian $\times1.45$')
#plt.loglog(ff, myswhite, 'k', lw=2,
#    label=r'Mystery white sensing')
#plt.loglog(ff, mysf2, c=(0.5, 0.5, 0.5), lw=2,
#    label=r'Mystery $1/f^2$ disp')
#plt.loglog(ff, np.sqrt((1.45*cbr)**2+myswhite**2+mysf2**2), c=(0.6, 0.6, 0), lw=2)
plt.grid('on', which='both')
plt.xlim(30, 5e3)
plt.ylim(6e-21, 5e-19)
plt.xlabel('Frequency [Hz]')
plt.ylabel(r'ASD of displacement $\bigl[\mathrm{m/Hz}^{1/2}\bigr]$')
plt.legend(loc='upper center', fancybox=True, framealpha=0.7,
        fontsize=10)
#plt.title('H1 freerunning DARM, 2015--12--02 05:35:00 Z')
plt.tight_layout()
plt.savefig('Figures/DARM/DARM_sum_null_cBr_2015-12-02_nom.pdf')
